#include <stdint.h>
#include <stdio.h>
#include "cpu.h"

/********************************************//**
 * \brief Таблица векторов прерываний
 *
 * \param  vect - адрес вектора (адрес где хранится адрес обработчика прерывания)
 * \param  nmi - Бит указатель на то является данное прерывание маскированым (maskable), если 1 то бит GIE не влияет на прерывание.
 ***********************************************/

#if(IRQ_USE)
const msp430_irq_vector_t msp430_irq_vectors[16] =
	{
		{.vect=0xffe0,.nmi=0},  //irq0
		{.vect=0xffe2,.nmi=0},  //irq1
		{.vect=0xffe4,.nmi=0},  //irq2
		{.vect=0xffe6,.nmi=0},  //irq3
		{.vect=0xffe8,.nmi=0},  //irq4
		{.vect=0xffea,.nmi=0},  //irq5
		{.vect=0xffec,.nmi=0},  //irq6
		{.vect=0xffee,.nmi=0},  //irq7
		{.vect=0xfff0,.nmi=0},  //irq8
		{.vect=0xfff2,.nmi=0},  //irq9
		{.vect=0xfff4,.nmi=0},  //irq10
		{.vect=0xfff6,.nmi=0},  //irq11
		{.vect=0xfff8,.nmi=0},  //irq12
		{.vect=0xfffa,.nmi=0},  //irq13 //OS Messages
		{.vect=0xfffc,.nmi=1},  //IO_FAULT_IRQ
		{.vect=0xfffe,.nmi=1}  //RST_IRQ
	};

void msp430_interrupt(msp430_context_t* cpu,uint8_t num)
	{
	cpu->irq.IF_REG |= (1<<num);
	cpu->irq.IE_REG |= (1<<num);
	}
#endif


/********************************************//**
 * \brief Начальная инициализация переменных
 *
 * \param Контекст процессора (msp430_context_t*)
 ***********************************************/
void msp430_init(msp430_context_t* cpu)
	{
#if(IRQ_USE)
	cpu->PC_REG = msp430_io(cpu,msp430_irq_vectors[15].vect,0,false,false);
#else
    cpu->PC_REG = msp430_io(cpu,0xfffe,0,false,false);
#endif

	debug_print(DEBUG_INIT_MODULE,"PC_REG = 0x%x\r\n",cpu->PC_REG);
	}


/********************************************//**
 * \brief Функция мультиплексора и выполнения команд
 *
 * \param Контекст процессора
 ***********************************************/
void msp430_cpu(msp430_context_t* cpu)
	{
	///< Получение команды из памяти по адрессу из PC_REG
	cpu->cmd = msp430_io(cpu,cpu->PC_REG,0,false,false);
	debug_print(DEBUG_CPU_MODULE,"cpu run: { PC_REG = 0x%x; SP_REG = 0x%x; SR_REG = 0x%x; CMD = 0x%x }\r\n",cpu->PC_REG,cpu->SP_REG,cpu->SR_REG,swap_bytes(cpu->cmd));
        
	switch(((msp430_cmd_op_t*) &cpu->cmd)->op)
		{
		///< Однооперандные команды
		case CMD_SINGLE_OP_NUM:
			{
			///< Сборка данных команды из двух половинок
			///< так вышло что невозможно выравнивание в структуре 12 бит, по этому используется два выравнивания 8 и 4.
			uint16_t uint_cmd_data = constr_16t(((msp430_cmd_op_t*) &cpu->cmd)->data_msb,((msp430_cmd_op_t*) &cpu->cmd)->data_lsb);
			///< Выбока кода команды. (opcode)
			///< Та же история про выравнивание в структуре
			uint8_t opcode = (((msp430_cmd_single_op_t*)&uint_cmd_data)->opcode_0<<1|((msp430_cmd_single_op_t*)&uint_cmd_data)->opcode_1);

			uint16_t operand = 0;

			///< Чтение данных
			if(opcode!= RETI)
				{
				operand =  msp430_addr_src_mode(cpu,
				                                ((msp430_cmd_single_op_t*)&uint_cmd_data)->as,
				                                ((msp430_cmd_single_op_t*)&uint_cmd_data)->reg,
				                                0,
				                                false,
				                                ((msp430_cmd_single_op_t*)&uint_cmd_data)->bw,
				                                opcode);
				}
			switch(opcode)
				{
					/********************************************//**
				* \brief Сдвиг вправо
				*
				* Если операнд >0 (<8000) и С = 1 то V=1 иначе 0 // если был плюс стал минус или был минус стал плюс
				* C принимает значение первого бита операнда (до сдвига)
				* Сдвиг вправо значения из регистра
				* Старший бит в результате берется из С статус-бита
				* Если Результат <0 (негативный) (>0x8000) то N = 1 иначе 0
				* Если результат 0, флаг Z = 1 иначе 0
				***********************************************/
				case RRC:
					{
					///< действие
					uint16_t tmp = operand;
					operand=operand>>1;
                                        if(cpu->SR_REG_bits.C)
                                          {
                                          operand |= (uint16_t) (1<<15);
                                          }
                                        else
                                          {
                                          operand&= ~(1<<15);
                                          }
					///< Провера битов статуса
					cpu->SR_REG_bits.V =  ((tmp>0)&&(tmp<check_bw(((msp430_cmd_single_op_t*)&uint_cmd_data)->bw,0x8000))&&cpu->SR_REG_bits.C);
					cpu->SR_REG_bits.C = (tmp&1);
					cpu->SR_REG_bits.N = check_n_bit(operand,(((msp430_cmd_single_op_t*)&uint_cmd_data)->bw));
					cpu->SR_REG_bits.Z = check_z_bit(operand);

					///< Запись результата
					msp430_addr_src_mode(cpu,
					                     ((msp430_cmd_single_op_t*)&uint_cmd_data)->as,
					                     ((msp430_cmd_single_op_t*)&uint_cmd_data)->reg,
					                     operand,
					                     true,
					                     ((msp430_cmd_single_op_t*)&uint_cmd_data)->bw, opcode);
					};
				break;
					/********************************************//**
				* \brief Перестановка байт
				* операнд[0..7] = результат[8..15]
				* операнд[8..15] = результат[0..7]
				* Биты статуса не изменяются
				***********************************************/
				case SWPB:
					{
					///< действие
					uint16_t temp = operand;
					operand = operand<<8;
					operand|= temp>>8;
					///< Запись результата
					msp430_addr_src_mode(cpu,
					                     ((msp430_cmd_single_op_t*)&uint_cmd_data)->as,
					                     ((msp430_cmd_single_op_t*)&uint_cmd_data)->reg,
					                     operand,
					                     true,
					                     ((msp430_cmd_single_op_t*)&uint_cmd_data)->bw,
					                     opcode);
					};
				break;
					/********************************************//**
				* \brief Сдвиг вправо без переноса
				*
				* Сдвиг вправо значения из регистра
				* Если результат 0, флаг Z = 1 иначе 0
				* Если Результат <0 (негативный) (>0x8000) то N = 1 иначе 0
				* V cбрасывается (V = 0);
				* C принимает значение первого бита до сдвига
				* Старший бит в результате берется из старшего бита операнда
				***********************************************/
				case RRA:
					{
					///< действие
					cpu->SR_REG_bits.C = (operand&1);
					operand=operand>>1;
					operand|=(operand&check_bw(((msp430_cmd_single_op_t*)&uint_cmd_data)->bw,0x4000))<<1;

					///< Провера битов статуса
					cpu->SR_REG_bits.V = 0;
					cpu->SR_REG_bits.N = check_n_bit(operand,((msp430_cmd_single_op_t*)&uint_cmd_data)->bw);
					cpu->SR_REG_bits.Z = check_z_bit(operand);

					///< Запись результата
					msp430_addr_src_mode(cpu,
					                     ((msp430_cmd_single_op_t*)&uint_cmd_data)->as,
					                     ((msp430_cmd_single_op_t*)&uint_cmd_data)->reg,
					                     operand,
					                     true,
					                     ((msp430_cmd_single_op_t*)&uint_cmd_data)->bw,
					                     opcode);
					};
				break;

					/********************************************//**
				* \brief Команда SXT Распространение знака
				*
				* Заполнение значением операнд[7] всех вышестояших битов результата
				* Если Результат <0 (негативный) (>0x8000) то N = 1 иначе 0
				* Если результат 0, флаг Z = 1 иначе 0
				* V cбрасывается (V = 0);
				* Если результат != 0 то C=1 иначе 0
				***********************************************/
				case SXT:
					{
					///< действие
					operand=((operand&0x80)?(operand|0xff00):(operand&0xff));

					///< Провера битов статуса
					cpu->SR_REG_bits.N = check_n_bit(operand,((msp430_cmd_single_op_t*)&uint_cmd_data)->bw);
					cpu->SR_REG_bits.Z = check_z_bit(operand);
					cpu->SR_REG_bits.C = ~check_z_bit(operand);
					cpu->SR_REG_bits.V = 0;

					///< Запись результата
					msp430_addr_src_mode(cpu,
					                     ((msp430_cmd_single_op_t*)&uint_cmd_data)->as,
					                     ((msp430_cmd_single_op_t*)&uint_cmd_data)->reg,
					                     operand,
					                     true,
					                     ((msp430_cmd_single_op_t*)&uint_cmd_data)->bw,
					                     opcode);
					};
				break;
					/********************************************//**
				* \brief Помещение данных в стек
				*
				* Биты статуса не изменяются
				* SP_REG уменьшается на 2 (стек стоит в верхней границе адреса ОЗУ)
				* После уменьшения указателся стека (SP_REG) данные пишутся в ОЗУ
				***********************************************/
				case PUSH:
					{
					///< Отнимание слова из регистра стека (уменьшение на размер слова)
					cpu->SP_REG -=2;
					///< Запись данных по адресу из стека
					msp430_io(cpu,cpu->SP_REG,operand,true,false);
					};
				break;
					/********************************************//**
				* \brief Команда безусловного перехода
				*
				* Биты статуса не изменяются
				* Вызов подпрограммы может производиться по любому адресу в пределах 64 кБайт адресного пространства.
				* Могут использоваться все способы адресации. Адрес возврата (адрес следующей команды) сохраняется в стеке.
				* Команда вызова подпрограммы – это команда-слово.
				***********************************************/
				case CALL:
					{
					///> Добавление адреса в стек
					cpu->SP_REG -=2;
					///> Запись по адрессу из стека данных PC
					msp430_io(cpu,cpu->SP_REG,cpu->PC_REG+2,true,false);
					///> Переход по адрессу операнда команды
					cpu->PC_REG = operand-2;
					};
				break;
					/********************************************//**
				* \brief  Команда возврата из прерывания
				*
				* Биты статуса не изменяются (восстанавливается SR_REG из стека)
				* Возвращение из стека PCREG и SRREG
				* http://www.ti.com/lit/ug/slau049f/slau049f.pdf страница 28 пункт 2.2.3
				***********************************************/
				case RETI:
					{
                    #if(IRQ_USE)
					///> Отнимание из стека (удаление, вверх)
					cpu->SP_REG +=2;
					///> Чтение из адресса по адрессу стека, возврат SR со стека
					cpu->SR_REG = msp430_io(cpu,cpu->SP_REG,0,false,false);

					///> Отнимание из стека (удаление, вверх)
					cpu->SP_REG +=2;
					///> Чтение из адресса по адрессу стека, возврат PC со стека
					cpu->PC_REG = msp430_io(cpu,cpu->SP_REG,0,false,false)-2;
					#endif
					}
				break;
				default:
					break;
				}
			};
		break;
///< Команды безусловоного перехода; Условный переход; PC = PC + 2×offset
		case CMD_COND_JUMP_NUM_0:
		case CMD_COND_JUMP_NUM_1:
			{
			///<Постройка и вычисление смещение перехода
			uint16_t offset_jmp = constr_16t(
			                          ((msp430_cmd_cond_jump_op_t*) &cpu->cmd)->offset_msb,
			                          ((msp430_cmd_cond_jump_op_t*) &cpu->cmd)->offset_lsb);
			///< Переход может быть как в большую сторону, так и в меньшую. 10 бит - бит знака.
			if(offset_jmp&(1<<9))
				{
				///< Переход на меньший адресс относительно PC_REG
				offset_jmp =  (cpu->PC_REG - ((((~offset_jmp)+1)-ROM_START_ADDR)*2));
				}
			else
				{
				///< Переход на больший адресс относительно PC_REG
				offset_jmp = (cpu->PC_REG+(offset_jmp*2));
				}

			///< Мультиплексор комманд
			switch(((msp430_cmd_cond_jump_op_t*) &cpu->cmd)->cond)
				{
					/********************************************//**
				* \brief Команда JNE/JNZ Переход если не ноль
				*
				* Проверяет статус бит Z и если он 0 то к счетчику команд (PC) добавляется смешение
				* Иначе PC=PC+2
				***********************************************/
				case JNEZ:
					{
					if(!cpu->SR_REG_bits.Z)
						{
						cpu->PC_REG = offset_jmp;
						}
					}
				break;
					/********************************************//**
				* \brief Команда JEQ/JZ Переход если равно/ноль
				*
				* Проверяет статус бит Z и если он 1 то к счетчику команд (PC) добавляется смешение
				* Иначе PC=PC+2
				***********************************************/
				case JEQZ:
					{
					if(cpu->SR_REG_bits.Z)
						{
						cpu->PC_REG = offset_jmp;
						}
					}

				break;
					/********************************************//**
				* \brief Команда JNC/JLO Переход если не_перенос/ниже (беззнаковое сравнение)
				*
				* Проверяет статус бит C и если он 0 то к счетчику команд (PC) добавляется смешение
				* Иначе PC=PC+2
				***********************************************/
				case JNCLO:
					{
					if(!cpu->SR_REG_bits.C)
						{
						cpu->PC_REG =  offset_jmp;
						}
					}

				break;
					/********************************************//**
				* \brief  Команда JC/JHS Переход если перенос/выше или то же (беззнаковое сравнение)
				*
				* Проверяет статус бит C и если он 1 то к счетчику команд (PC) добавляется смешение
				* Иначе PC=PC+2
				***********************************************/
				case JCHS:
					{
					if(cpu->SR_REG_bits.C)
						{
						cpu->PC_REG = offset_jmp;
						}
					}

				break;
					/********************************************//**
				* \brief Команда JN Переход если отрицательный
				*
				* Проверяет статус бит N и если он 1 то к счетчику команд (PC) добавляется смешение
				* Иначе PC=PC+2
				***********************************************/
				case JN:
					{
					if(cpu->SR_REG_bits.N)
						{
						cpu->PC_REG = offset_jmp;
						}
					}
				break;
					/********************************************//**
				* \brief Команда JGE Переход если больше_или_равно
				*
				* Проверяет статус бит N ^ V = 0 (N.xor.V=0) то к счетчику команд (PC) добавляется смешение
				* Иначе PC=PC+2
				***********************************************/
				case JGE:
					{
					if(!((cpu->SR_REG_bits.N)^(cpu->SR_REG_bits.V)))
						{
						cpu->PC_REG = offset_jmp;
						}
					}
				break;
					/********************************************//**
				* \brief Команда JL Переход если меньше
				*
				* Проверяет статус бит N ^ V = 1 (N.xor.V=1) то к счетчику команд (PC) добавляется смешение
				* Иначе PC=PC+2
				***********************************************/
				case JL:
					{
					if((cpu->SR_REG_bits.N)^(cpu->SR_REG_bits.V))
						{
						cpu->PC_REG = offset_jmp;
						}
					}
				break;
					/********************************************//**
				* \brief  Команда JMP Переход (непосредственный)
				*
				* к счетчику команд (PC) добавляется смешение
				***********************************************/
				case JMP:
					cpu->PC_REG = offset_jmp;
					break;
				default:
					break;
				}
			};
		break;
///< Команды Двух операндной арифметики
		default:
			{
			uint16_t uint_cmd_data = constr_16t(((msp430_cmd_op_t*) &cpu->cmd)->data_msb,((msp430_cmd_op_t*) &cpu->cmd)->data_lsb);
			///< Получение данных из источника данных (src, source)
			uint16_t src = msp430_addr_src_mode(cpu,
			                                    ((msp430_cmd_two_op_t*)&uint_cmd_data)->as,
			                                    ((msp430_cmd_two_op_t*)&uint_cmd_data)->src,
			                                    0,
			                                    false,
			                                    ((msp430_cmd_two_op_t*)&uint_cmd_data)->bw,
			                                    ((((msp430_cmd_op_t*) &cpu->cmd)->op)<<1)
			                                   );

			///< получение данных из места назначения (dst; destination)
			///< Данные из dst используются во всех командах кроме MOV,
			///< что бы не делать дополнительные вызовы во всех ветках switch(cmd) было вынесенно сюда
			uint16_t dst = 0;
			if((((msp430_cmd_op_t*) &cpu->cmd)->op)!=MOV)
				{
				dst = msp430_addr_dst_mode(cpu,
				                           ((msp430_cmd_two_op_t*)&uint_cmd_data)->ad,
				                           ((msp430_cmd_two_op_t*)&uint_cmd_data)->dest,
				                           0,
				                           false,
				                           ((bool)((msp430_cmd_two_op_t*)&uint_cmd_data)->bw));
				}

			switch((((msp430_cmd_op_t*) &cpu->cmd)->op))
				{
					/********************************************//**
				* \brief  Команда MOV Переслать источник в приёмник
				*
				* dst = src
				* Статусные биты не меняются
				***********************************************/
				case MOV:
					{
					///< Запись данных в dst
					msp430_addr_dst_mode(cpu,
					                     ((msp430_cmd_two_op_t*)&uint_cmd_data)->ad,
					                     ((msp430_cmd_two_op_t*)&uint_cmd_data)->dest,
					                     src,
					                     true,
					                     ((bool)((msp430_cmd_two_op_t*)&uint_cmd_data)->bw));
					};
				break;
					/********************************************//**
				* \brief Команда ADD Прибавить источник к приёмнику
				*
				* dst = dst+src;
				*
				* Если Результат <0 (негативный) (>0x7fff) то N = 1 иначе 0
				* Если результат 0, флаг Z = 1 иначе 0
				* Если сумма двух положительных чисел отрицательное число, либо сумма двух отрицательных чисел положительна флаг V = 1 иначе 0
				* Если результат dst>src то С = 1 иначе 0
				*
				***********************************************/
				case ADD:
					{
					///< Действие над операндами
					uint16_t operand = src+dst;

					///< Установка статус битов
					cpu->SR_REG_bits.N = check_n_bit(operand,((msp430_cmd_two_op_t*)&uint_cmd_data)->bw);
					cpu->SR_REG_bits.Z = check_z_bit(operand);
					cpu->SR_REG_bits.V = check_v_bit(src,dst,operand,((msp430_cmd_two_op_t*)&uint_cmd_data)->bw);
					cpu->SR_REG_bits.C = check_c_bit(dst,operand,((msp430_cmd_two_op_t*)&uint_cmd_data)->bw);

					///< Запись данных в dst
					msp430_addr_dst_mode(cpu,
					                     ((msp430_cmd_two_op_t*)&uint_cmd_data)->ad,
					                     ((msp430_cmd_two_op_t*)&uint_cmd_data)->dest,
					                     operand,
					                     true,
					                     ((bool)((msp430_cmd_two_op_t*)&uint_cmd_data)->bw));
					};
				break;
					/********************************************//**
				* \brief Команда ADDC Прибавить источник к приёмнику
				*
				* dst = dst+src+C;
				*
				* Если Результат <0 (негативный) (>0x7fff) то N = 1 иначе 0
				* Если результат 0, флаг Z = 1 иначе 0
				* Если сумма двух положительных чисел отрицательное число, либо сумма двух отрицательных чисел положительна флаг V = 1 иначе 0
				* Если результат src-dst отрицательно (src или dst больше 0x3fff) то С = 1 иначе 0
				*
				***********************************************/
				case ADDC:
					{
					///< Действие над операндами
					uint16_t operand = src+dst+cpu->SR_REG_bits.C;

					///< Установка статус битов
					cpu->SR_REG_bits.N = check_n_bit(operand,((msp430_cmd_two_op_t*)&uint_cmd_data)->bw);
					cpu->SR_REG_bits.Z = check_z_bit(operand);
					cpu->SR_REG_bits.V = check_v_bit(src,dst,operand,((msp430_cmd_two_op_t*)&uint_cmd_data)->bw);
					cpu->SR_REG_bits.C = check_c_bit(dst,operand,((msp430_cmd_two_op_t*)&uint_cmd_data)->bw);

					///< Запись данных в dst
					msp430_addr_dst_mode(cpu,
					                     ((msp430_cmd_two_op_t*)&uint_cmd_data)->ad,
					                     ((msp430_cmd_two_op_t*)&uint_cmd_data)->dest,
					                     operand,
					                     true,
					                     ((bool)((msp430_cmd_two_op_t*)&uint_cmd_data)->bw));
					};
				break;
					/********************************************//**
				* \brief Команда SUBC Вычесть источник из приёмника (с переносом)
				*
				* dst = dst + ~src + C
				* Если Результат <0 (негативный) (>0x7fff) то N = 1 иначе 0
				* Если результат 0, флаг Z = 1 иначе 0

				* Если Сумма отрицательного - положительно = положительно, либо положительно - отрицательно = отрицательно (если меняется знак) то V = 1 иначе 0
				* Если результат src-dst отрицательно (src или dst больше 0x3fff) то С = 1 иначе 0
				*
				***********************************************/

				case SUBC:
					{
					///< Действие над операндами
					uint16_t operand = (~src)+dst+cpu->SR_REG_bits.C;

					///< Установка статус битов
					cpu->SR_REG_bits.N = check_n_bit(operand,((msp430_cmd_two_op_t*)&uint_cmd_data)->bw);
					cpu->SR_REG_bits.Z = check_z_bit(operand);
					cpu->SR_REG_bits.V = check_v_bit(((check_bw(((msp430_cmd_two_op_t*)&uint_cmd_data)->bw,0xffff)-src)-src),dst,operand,((msp430_cmd_two_op_t*)&uint_cmd_data)->bw);
					cpu->SR_REG_bits.C = check_c_bit(dst,operand,((msp430_cmd_two_op_t*)&uint_cmd_data)->bw);

					///< Запись данных в dst
					msp430_addr_dst_mode(cpu,
					                     ((msp430_cmd_two_op_t*)&uint_cmd_data)->ad,
					                     ((msp430_cmd_two_op_t*)&uint_cmd_data)->dest,
					                     operand,
					                     true,
					                     ((bool)((msp430_cmd_two_op_t*)&uint_cmd_data)->bw));
					};
				break;
					/********************************************//**
				* \brief Команда SUB Вычесть источник из приёмника
				*
				* dst = dst + ~src+1 (dst = dst-src)
				* Если Результат <0 (негативный) (>0x7fff) то N = 1 иначе 0
				* Если результат 0, флаг Z = 1 иначе 0

				* Если Сумма отрицательного - положительно = положительно, либо положительно - отрицательно = отрицательно (если меняется знак) то V = 1 иначе 0
				* Если dst не равен 0  и src = 0xffff то С = 1 иначе 0
				*
				***********************************************/
				case SUB:
					{
					///< Действие над операндами
					uint16_t operand = dst-src;

					///< Установка статус битов
					cpu->SR_REG_bits.N = check_n_bit(operand,((msp430_cmd_two_op_t*)&uint_cmd_data)->bw);
					cpu->SR_REG_bits.Z = check_z_bit(operand);
					cpu->SR_REG_bits.V = check_v_bit(((check_bw(((msp430_cmd_two_op_t*)&uint_cmd_data)->bw,0xffff)-src)-src),dst,operand,((msp430_cmd_two_op_t*)&uint_cmd_data)->bw);  ///<- у меня такое подозрение что компилятор урезает ~src, с отдельной переменной работает, с ffff-src тоже, с ~src нет
					cpu->SR_REG_bits.C = check_c_bit(dst,operand,((msp430_cmd_two_op_t*)&uint_cmd_data)->bw);

					///< Запись данных в dst
					msp430_addr_dst_mode(cpu,
					                     ((msp430_cmd_two_op_t*)&uint_cmd_data)->ad,
					                     ((msp430_cmd_two_op_t*)&uint_cmd_data)->dest,
					                     operand,
					                     true,
					                     ((bool)((msp430_cmd_two_op_t*)&uint_cmd_data)->bw));
					};
				break;
					/********************************************//**
				* \brief Команда CMP Сравнить (операцией вычитания) источник с приёмником
				*
				* dst == src ->  (dst+(~src)+1)
				*
				* Если src>=dst то N = 1 иначе 0
				* Если src==dst, флаг Z = 1 иначе 0
				* Если Сумма отрицательного - положительно = положительно, либо положительно - отрицательно = отрицательно (если меняется знак) то V = 1 иначе 0
				* Если результат src-dst отрицательно (src или dst больше 0x3fff) то С = 1 иначе 0
				***********************************************/

				case CMP:
					{
					///< Действие над операндами
					uint16_t operand = dst - src;

					///< Установка статус битов
					cpu->SR_REG_bits.N = check_n_bit(operand,((msp430_cmd_two_op_t*)&uint_cmd_data)->bw);
					cpu->SR_REG_bits.Z = check_z_bit(operand);
					cpu->SR_REG_bits.V = check_v_bit((check_bw(((msp430_cmd_two_op_t*)&uint_cmd_data)->bw,0xffff)-src),dst,operand,((msp430_cmd_two_op_t*)&uint_cmd_data)->bw);
					cpu->SR_REG_bits.C = (dst>=operand);

					///< костыль, если адресация получателя (dst) абсолютная или символическа, т.е с PC+2 будут читать адрес памяти то добавить +2 к PC
					if(((msp430_cmd_two_op_t*)&uint_cmd_data)->ad)
						{
						cpu->PC_REG +=2;
						}
					};
				break;
					/********************************************//**
				* \brief Команда DADD Decimal Десятичное сложение источника и приёмника (с переносом)
				*
				* dst = bin2bcd ( bcd2bin(dst) + bcd2bin(src) + C );
				*
				* Если dst[MSB] = 1 то N = 1 иначе 0
				* Если результат 0, флаг Z = 1 иначе 0
				* Если результат больше >0x9999 (bcd) то C = 1 иначе 0
				* V остается таким каким был
				***********************************************/
				case DADD:
					{
					///< Действие над операндами
					uint16_t operand = bin2bcd ( bcd2bin(dst) + bcd2bin(src)+ cpu->SR_REG_bits.C );

					///< Установка статус битов
					cpu->SR_REG_bits.N = check_n_bit(operand,((msp430_cmd_two_op_t*)&uint_cmd_data)->bw);
					cpu->SR_REG_bits.Z = check_z_bit(operand);
					cpu->SR_REG_bits.C = (operand>check_bw(((msp430_cmd_two_op_t*)&uint_cmd_data)->bw,0x9999));

					///< Запись данных в dst
					msp430_addr_dst_mode(cpu,
					                     ((msp430_cmd_two_op_t*)&uint_cmd_data)->ad,
					                     ((msp430_cmd_two_op_t*)&uint_cmd_data)->dest,
					                     operand,
					                     true,
					                     ((bool)((msp430_cmd_two_op_t*)&uint_cmd_data)->bw));
					};
				break;
					/********************************************//**
				* \brief Команда BIT Проверка битов (операцией AND) источника и приёмника
				*
				*  dst & src
				* Если dst[MSB] = 1 то N = 1 иначе 0 (отрицательное)
				* Если результат 0, флаг Z = 1 иначе 0
				* Если результат  не 0, флаг С = 1 иначе 0
				* V = 0;
				***********************************************/
				case BIT_CMD:
					{
					///< Действие над операндами
					uint16_t operand = dst & src;

					///< Установка статус битов
					cpu->SR_REG_bits.N = check_n_bit(operand,((msp430_cmd_two_op_t*)&uint_cmd_data)->bw);
					cpu->SR_REG_bits.Z = check_z_bit(operand);
					cpu->SR_REG_bits.C = ~check_z_bit(operand);
					cpu->SR_REG_bits.V = 0;

					///< костыль, если адресация получателя (dst) абсолютная или символическа, т.е с PC+2 будут читать адрес памяти то добавить +2 к PC
					if(((msp430_cmd_two_op_t*)&uint_cmd_data)->ad)
						{
						cpu->PC_REG +=2;
						}
					};
				break;
					/********************************************//**
				* \brief Команда BIC Битовая очистка (dest &= ~src)
				*
				* dst = dst & (~src) ;
				*
				* Биты статуса не изменяются
				***********************************************/

				case BIC:
					{
					///< Действие над операндами
					uint16_t operand = dst & (~src);

					///< Запись данных в dst
					msp430_addr_dst_mode(cpu,
					                     ((msp430_cmd_two_op_t*)&uint_cmd_data)->ad,
					                     ((msp430_cmd_two_op_t*)&uint_cmd_data)->dest,
					                     operand,
					                     true,
					                     ((bool)((msp430_cmd_two_op_t*)&uint_cmd_data)->bw));
					};
				break;
					/********************************************//**
				* \brief Команда BIS Битовая установка (logical OR)
				*
				* dst = dst | src
				* Биты статуса не изменяются
				***********************************************/
				case BIS:
					{
					///< Действие над операндами
					uint16_t operand = dst | src;

					///< Запись данных в dst
					msp430_addr_dst_mode(cpu,
					                     ((msp430_cmd_two_op_t*)&uint_cmd_data)->ad,
					                     ((msp430_cmd_two_op_t*)&uint_cmd_data)->dest,
					                     operand,
					                     true,
					                     ((bool)((msp430_cmd_two_op_t*)&uint_cmd_data)->bw));
					};
				break;
					/********************************************//**
				* \brief Команда XOR Исключающее или источника с приёмником
				*
				* dst = dst ^ src
				*
				* Если dst[MSB] = 1 то N = 1 иначе 0 (отрицательное)
				* Если результат 0, флаг Z = 1 иначе 0
				* Если результат  не 0, флаг С = 1 иначе 0
				* Если src > 0x7fff и dst > 0x7fff то V = 1 иначе 0
				***********************************************/
				case XOR:
					{
					///< Действие над операндами
					uint16_t operand = dst ^ src;

					///< Установка статус битов
					cpu->SR_REG_bits.N = check_n_bit(operand,((msp430_cmd_two_op_t*)&uint_cmd_data)->bw);
					cpu->SR_REG_bits.Z = check_z_bit(operand);
					cpu->SR_REG_bits.C = ~check_z_bit(operand);
					cpu->SR_REG_bits.V = ((dst&check_bw(((msp430_cmd_two_op_t*)&uint_cmd_data)->bw,0x8000))&&(src&check_bw(((msp430_cmd_two_op_t*)&uint_cmd_data)->bw,0x8000)));

					///< Запись данных в dst
					msp430_addr_dst_mode(cpu,
					                     ((msp430_cmd_two_op_t*)&uint_cmd_data)->ad,
					                     ((msp430_cmd_two_op_t*)&uint_cmd_data)->dest,
					                     operand,
					                     true,
					                     ((bool)((msp430_cmd_two_op_t*)&uint_cmd_data)->bw));
					};
				break;
					/********************************************//**
				* \brief  Команда AND Логический AND источника с приёмником (dest &= src)
				*
				* dst = dst & src
				* Если dst[MSB] = 1 то N = 1 иначе 0 (отрицательное)
				* Если результат 0, флаг Z = 1 иначе 0
				* Если результат  не 0, флаг С = 1 иначе 0
				* V = 0;
				***********************************************/
				case AND:
					{
					///< Действие над операндами
					uint16_t operand = dst & src;
					///< Установка статус битов
					cpu->SR_REG_bits.N = check_n_bit(operand,((msp430_cmd_two_op_t*)&uint_cmd_data)->bw);
					cpu->SR_REG_bits.Z = check_z_bit(operand);
					cpu->SR_REG_bits.C = ~check_z_bit(operand);
					cpu->SR_REG_bits.V = 0;

					///< Запись данных в dst
					msp430_addr_dst_mode(cpu,
					                     ((msp430_cmd_two_op_t*)&uint_cmd_data)->ad,
					                     ((msp430_cmd_two_op_t*)&uint_cmd_data)->dest,
					                     operand,
					                     true,
					                     ((bool)((msp430_cmd_two_op_t*)&uint_cmd_data)->bw));
					};
				break;

				default:
					break;
				}
			};
		break;
		}
	///< Переход к следующей комманде
	cpu->PC_REG+=2;
#if(IRQ_USE)
	///< Вызов прерываний, если такие имеются (IF_REG & IE_REG)
	///< Описание Slau049f (http://www.ti.com/lit/ug/slau049f/slau049f.pdf) пункт 2.2.3
	///< 1. Завершена текущая инструкция
	if(cpu->irq.IF_REG & cpu->irq.IE_REG)
		{
		///< 4. Выборка старшего по приоритету прерывания (15 самое старшое, 0 младшее)
		uint8_t i = 16;
		for(; i-- > 0;)
			{
			if((cpu->irq.IF_REG & cpu->irq.IE_REG) & (1<<i))
				{
				///< Маскировка прерывания по GIE
				if(msp430_irq_vectors[i].nmi||cpu->SR_REG_bits.GIE)
					{
					///< 2. PC должен быть помещен в верхушку стека
					msp430_io(cpu,cpu->SP_REG,cpu->PC_REG,true,false);
					cpu->SP_REG -=2;
					///< 3. SR должен быть помещен в верхушку стека
					msp430_io(cpu,cpu->SP_REG,cpu->SR_REG,true,false);
					cpu->SP_REG -=2;
					///< 6. Сброс SR регистра
					cpu->SR_REG = 0;
					///< 7. установка PC_REG в вектор прерывания
					cpu->PC_REG = msp430_io(cpu,msp430_irq_vectors[i].vect,0,false,false);
					}
				///< 5. Должны быть сброшены флаги запроса прерывания (IFG биты (пример: WDTIFG))
				cpu->irq.IF_REG &=~(1<<i);
				}
			}

		}
#endif

	}

/********************************************//**
 * \brief Функция памяти для ядра MSP430,
 *
 * \param Параметр addr передает адрес в памяти, с которым будет производится дейтсвие
 * \param Параметр data передает данные в память при записи (write=true)
 * \param Параметр write передает действия функции, при write = true
 * \return Возвращает данные с памяти, при write = false иначе 0
 *
 ***********************************************/

uint16_t msp430_io(msp430_context_t* cpu,uint16_t addr, uint16_t data ,bool write,bool bw)
	{
	if(bw)
		{
		data &= 0xff;
		}
	///< SFR
	if(addr<=SFR_END_ADDR)
		{
#if(IRQ_USE)
		if(write)
			{
			debug_print(DEBUG_IO_MODULE,"SFR::WRITE {addr: 0x%x data: 0x%x}\r\n",addr,data);
			if(bw)
				{
				cpu->irq.SFR[addr] = data;
				}
			else
				{
				cpu->irq.SFR[addr] = data;
				cpu->irq.SFR[addr+1] = (data>>8)&0xff;
				}
			}
		else
			{
			if(bw)
				{
				debug_print(DEBUG_IO_MODULE,"SFR::READ (byte) {addr: 0x%x data: 0x%x}\r\n",addr,cpu->irq.SFR[addr]);
				return cpu->irq.SFR[addr]&0xff;
				}
			else
				{
				debug_print(DEBUG_IO_MODULE,"SFR::READ (word) {addr: 0x%x data: 0x%x}\r\n",addr,(cpu->irq.SFR[addr])|(cpu->irq.SFR[addr+1]<<8));
				return (cpu->irq.SFR[addr])|(cpu->irq.SFR[addr+1]<<8);
				}
			}
#endif
		return 0x0;
		}
#if(IO_USE_CALLBACKS)
	if((addr>=IO_8BIT_START_ADDR)&&(addr<=IO_16BIT_END_ADDR))
		{
		if(write)
			{
			debug_print(DEBUG_IO_MODULE,"IO8/16::WRITE {addr: 0x%x data: 0x%x}\r\n",addr,data);
			cpu->io_write_cb(addr,data);
			return 0;
			}
		else
			{
			debug_print(DEBUG_IO_MODULE,"IO8/16::READ {addr: 0x%x data: 0x%x}\r\n",addr,cpu->io_read_cb(addr));
			return cpu->io_read_cb(addr);
			}
		}
#else
	///< адреса 8 битной переферии
	if((addr>=IO_8BIT_START_ADDR)&&(addr<=IO_8BIT_END_ADDR))
		{
		debug_print(DEBUG_IO_MODULE,write?"IO8::WRITE {addr: 0x%x data: 0x%x}\r\n":"IO8::READ {addr: 0x%x data: 0x%x}\r\n",addr,data);
		return 0x0;
		}
	///< адреса 16 битной переферии
	if((addr>=IO_16BIT_START_ADDR)&&(addr<=IO_16BIT_END_ADDR))
		{
		debug_print(DEBUG_IO_MODULE,write?"IO16::WRITE {addr: 0x%x data: 0x%x}\r\n":"IO16::READ {addr: 0x%x data: 0x%x}\r\n",addr,data);
		return 0;
		}
#endif
	///< адреса ОЗУ
	if((addr>=RAM_START_ADDR)&&(addr<=RAM_END_ADDR))
		{
		if(write)
			{
			debug_print(DEBUG_IO_MODULE,"RAM::WRITE {addr: 0x%x data: 0x%x}\r\n",addr,data);
			if(bw)
				{
#if(RAM_USE_CALLBACKS)
				cpu->ram_write_cb(addr-RAM_START_ADDR,data);
				return 0;
#else
				cpu->ram[addr-RAM_START_ADDR] = data;
				return 0;
#endif
				}
			else
				{
#if(RAM_USE_CALLBACKS)
				cpu->ram_write_cb(addr-RAM_START_ADDR,data&0xff);
				cpu->ram_write_cb(addr-RAM_START_ADDR+1,(data>>8)&0xff);
				return 0;
#else
				cpu->ram[addr-RAM_START_ADDR] = data&0xff;
				cpu->ram[addr-RAM_START_ADDR+1] = (data>>8)&0xff;
				return 0;
#endif
				}
			}
		else
			{
			if(bw)
				{
#if(RAM_USE_CALLBACKS)
				debug_print(DEBUG_IO_MODULE,"RAM::READ (byte) {addr: 0x%x data: 0x%x}\r\n",addr,cpu->ram_read_cb(addr-RAM_START_ADDR));
				return cpu->ram_read_cb(addr-RAM_START_ADDR);
#else
				debug_print(DEBUG_IO_MODULE,"RAM::READ (byte) {addr: 0x%x data: 0x%x}\r\n",addr,cpu->ram[addr-RAM_START_ADDR]);
				return cpu->ram[addr-RAM_START_ADDR];
#endif
				}
			else
				{
#if(RAM_USE_CALLBACKS)
				debug_print(DEBUG_IO_MODULE,"RAM::READ (word) {addr: 0x%x data: 0x%x}\r\n",addr,cpu->ram_read_cb(addr-RAM_START_ADDR)|(cpu->ram_read_cb(addr-RAM_START_ADDR+1)<<8));
				return cpu->ram_read_cb(addr-RAM_START_ADDR)|(cpu->ram_read_cb(addr-RAM_START_ADDR+1)<<8);
#else
				debug_print(DEBUG_IO_MODULE,"RAM::READ (word) {addr: 0x%x data: 0x%x}\r\n",addr,cpu->ram[addr-RAM_START_ADDR]|(cpu->ram[addr-RAM_START_ADDR+1]<<8));
				return cpu->ram[addr-RAM_START_ADDR]|(cpu->ram[addr-RAM_START_ADDR+1]<<8);
#endif
				}
			}
		}
	///< адреса програмы
	if(addr>=ROM_START_ADDR)
		{
		if(!write)
			{
			if(bw)
				{
#if(ROM_USE_CALLBACKS)
				debug_print(DEBUG_IO_MODULE,"ROM::READ (byte) {addr: 0x%x data: 0x%x}\r\n",addr,cpu->rom_read_cb(addr-ROM_START_ADDR));
				return cpu->rom_read_cb(addr-ROM_START_ADDR);
#else
				debug_print(DEBUG_IO_MODULE,"ROM::READ (byte) {addr: 0x%x data: 0x%x}\r\n",addr,cpu->rom[addr-ROM_START_ADDR]);
				return cpu->rom[addr-ROM_START_ADDR];
#endif
				}
			else
				{
#if(ROM_USE_CALLBACKS)
				debug_print(DEBUG_IO_MODULE,"ROM::READ (word) {addr: 0x%x data: 0x%x}\r\n",addr,cpu->rom_read_cb(addr-ROM_START_ADDR)|(cpu->rom_read_cb(addr-ROM_START_ADDR+1)<<8));
				return cpu->rom_read_cb(addr-ROM_START_ADDR)|(cpu->rom_read_cb(addr-ROM_START_ADDR+1)<<8);
#else
				debug_print(DEBUG_IO_MODULE,"ROM::READ (word) {addr: 0x%x data: 0x%x}\r\n",addr,cpu->rom[addr-ROM_START_ADDR]|(cpu->rom[addr-ROM_START_ADDR+1]<<8));
				return cpu->rom[addr-ROM_START_ADDR]|(cpu->rom[addr-ROM_START_ADDR+1]<<8);
#endif
				}
			}
		}
	return 0;
	}

/********************************************//**
 * \brief Функция селектора адресации операнда в который записывается данные (dst)
 *
 * Возможно два режима, задающихся через ad
 * Если ad = 0 то это прямая регистровая адресация (результат пишется в регистр)
 * Если ad = 1 то это индексная адресация и результат пишется в значение регистра + слова идущего после команды (PC+4)
 *
 * Если запись в регистр PC то значение операнда на 2 меньше, так как после выполнения функции добавляется 2 к PC
 *
 * \param ad - тип адресации операнда
 * \param reg_num - номер регистра с которым работает команда
 * \param data - данные для записи
 * \return ничего не возвращает
 ***********************************************/

uint16_t msp430_addr_dst_mode(msp430_context_t* cpu,uint8_t ad,uint8_t reg_num,uint16_t data, bool write, bool bw)
	{
	if(ad)
		{
		/********************************************//**
		 * \brief Абсолютный. Операнд в памяти по адресу взятому из x.
		 *
		 * происходит когда AD = 1 и регистр получателя (reg_num) = 2
		 ***********************************************/
		if(reg_num == 2)
			{
			if(write)
				{
				cpu->PC_REG +=2;
				msp430_io(cpu,msp430_io(cpu,cpu->PC_REG,0,false,false),data,true,bw);
				return 0;
				}
			else
				return msp430_io(cpu,msp430_io(cpu,cpu->PC_REG+2,0,false,false),0,false,bw);
			}
		/********************************************//**
		 * \brief
		 *
		 * Индексный. Операнд находится в памяти по адресу Rn+x. (reg[reg_num]+(@PC_REG+2))
		 * X-слово находится после текущей команды.
		 ***********************************************/
		else
			{
			if(write)
				{
				cpu->PC_REG +=2;
				msp430_io(cpu,cpu->reg[reg_num]+msp430_io(cpu,cpu->PC_REG,0,false,false),data,true,bw);
				return 0;
				}
			else
				{
				return msp430_io(cpu,cpu->reg[reg_num]+msp430_io(cpu,cpu->PC_REG+2,0,false,false),0,false,bw);
				}
			}

		}
	/********************************************//**
	 * \brief  	Регистровый. Операнд — содержимое одного из регистров из Rn.
	 *
	 * Операнд dst берется из регистра указаного в команде (reg_num)
	 * Происходит когда AD = 0
	 ***********************************************/
	else
		{
		if(write)
			{
			//cpu->reg[reg_num] = (bw)?(((reg_num==0)?(data-2):data)&0xff):((reg_num==0)?(data-2):data);
			if(bw)
				{
                                if(reg_num == 0)
                                  {
                                    cpu->reg[reg_num] = (data-2)&0xff;
                                  }
                                else
                                  {
                                    cpu->reg[reg_num] = (data)&0xff;
                                  }
				}
			else
				{
				//cpu->reg[reg_num] = ((reg_num==0)?(data-2):data);
                                if(reg_num == 0)
                                  {
                                    cpu->reg[reg_num] = (data-2);
                                  }
                                else
                                  {
                                    cpu->reg[reg_num] = data;
                                  }
				}
			return 0;
			}
		else
			{
			//return (bw)?(cpu->reg[reg_num]&0xff):cpu->reg[reg_num];
			if(bw)
				{
				return (cpu->reg[reg_num]&0xff);
				}
			else
				{
				return cpu->reg[reg_num];
				}
			}

		}
	}

/********************************************//**
 * \brief Функция селектора адресации операнда из которого считываются данные (src)
 *
 * Установка необходимого источника операнда
 *
 * \param AS - тип адресации
 * \param reg - задействованный регистр в команде
 * \param operand - операнд для записи, при чтение не используется
 * \param write указатель действия с операндом, если 1 то запись
 * \param bw параметр команды, работа с байтом или словом
 * \return При чтение возвращает значение операнда
 *
 ***********************************************/

uint16_t msp430_addr_src_mode(msp430_context_t* cpu,uint8_t as, uint8_t reg_num,uint16_t operand,bool write,uint8_t bw,uint8_t cmd_op)
	{
	switch(as)
		{
		case AS_00:
			{
			if(write)
				{
				//   cpu->reg[reg_num] = (bw)?(operand&0xff):(operand);
				if(bw)
					{
					cpu->reg[reg_num] = (operand&0xff);
					}
				else
					{
					cpu->reg[reg_num] = operand;
					}
				}
			else
				{
				///<Если AS = 0, и Номер регистра = 3 то возвращается константа 0
				///<Иначе это регистровая адресация, и возращается значение выбраного регистра
				//operand = (reg_num != 3 )?((bw)?(cpu->reg[reg_num]&0xff):cpu->reg[reg_num]):0;
				if(reg_num!=3)
					{
					if(bw)
						operand = (cpu->reg[reg_num]&0xff);
					else
						operand = cpu->reg[reg_num];
					}
				else
					{
					operand = 0;
					}
				}
			}
		break;
		case AS_01:
			{
			switch(reg_num)
				{
				///< Относительный(символьный). x(PC) Операнд в памяти по адресу PC+x.
				case 0:
					if(write)
						{
						cpu->PC_REG+=2;
						msp430_io(cpu,cpu->PC_REG + msp430_io(cpu,cpu->PC_REG,0,false,false),operand,true,(bool)(bw));
						}
					else
						{
						operand = cpu->PC_REG + msp430_io(cpu,cpu->PC_REG+2,0,false,false);
						if(cmd_op>7) cpu->PC_REG+=2;
						}
					break;
				///< Абсолютный. Операнд в памяти по адресу взятому из x.
				case 2:
					if(write)
						{
						msp430_io(cpu,msp430_io(cpu,cpu->PC_REG+2,0,false,false),operand,true,(bool)(bw));
						cpu->PC_REG+=2;
						}
					else
						{
						operand = msp430_io(cpu,msp430_io(cpu,cpu->PC_REG+2,0,false,false),0,false,(bool)(bw));
						if(cmd_op>7) cpu->PC_REG+=2;
						}

					break;
				///< Константа 1
				case 3:
					if(!write)
						operand = 1;
					break;
				///< Индексный. Операнд находится в памяти по адресу Rn+x.
				///< X-слово находится после текущей команды.
				default:
					if(write)
						{
						msp430_io(cpu,cpu->reg[reg_num]+msp430_io(cpu,cpu->PC_REG+2,0,false,false),operand,true,(bool)(bw));
						cpu->PC_REG+=2;
						}
					else
						{
						operand = msp430_io(cpu,cpu->reg[reg_num]+msp430_io(cpu,cpu->PC_REG+2,0,false,false),0,false,(bool)(bw));
						if(cmd_op>7) cpu->PC_REG+=2;
						}
					break;
				}
			}
		break;
		case AS_10:
			{
			switch(reg_num)
				{
				///< Невозможная операция
				case 0:
					break;
				///< Константа 4.
				case 2:
					if(!write) operand = 4;
					break;
				///< Константа 2
				case 3:
					if(!write) operand = 2;
					break;
				///< Косвенный регистровый
				///< Косвенный регистровый. Операнд находится в памяти по адресу, который содержится в регистре Rn.
				default:
					if(write)
						msp430_io(cpu,cpu->reg[reg_num],operand,true,(bool)(bw));
					else
						operand = msp430_io(cpu,cpu->reg[reg_num],0,false,(bool)(bw));
					break;
				}
			}
		break;
		case AS_11:
			{
			switch(reg_num)
				{
				///< Непосредственный. @PC+ Адрес операнда из х-слова находящегося после текущей команды.
				case 0:
					if(write)
						{
						cpu->PC_REG+=2;
						msp430_io(cpu,cpu->PC_REG,operand,true,(bool)(bw));
						}
					else
						{
						operand = msp430_io(cpu,cpu->PC_REG+2,0,false,(bool)(bw));
             //todo здесь происходит какая-та хурма
						if((cmd_op&8)||(cmd_op==5)||(cmd_op&0x10)) 
                                                    cpu->PC_REG+=2;
						}
					break;
				///< Константа 8.
				case 2:
					operand = 8;
					break;
				///< Константа −1 или 0xFFFF.
				case 3:
					operand = check_bw(bw,0xffff);
					break;
				///< Косвенный регистровый с автоинкрементом.
				///< Берется значение из памяти по адресу находяшемся в регистре,
				///< в него же и пишется, после операции содержимое регистра увеличивается на 1 если bw=1 и  на 2 если bw=0
				///< В зависимости от значения разряда B/W значение регистра Rn увеличивается после выполнения операции на 1 или 2.
				default:
					if(write)
						{
						msp430_io(cpu,cpu->reg[reg_num],operand,true,(bool)(bw));
						if(bw)
							{
							cpu->reg[reg_num]+=1;
							}
						else
							{
							cpu->reg[reg_num]+=2;
							}
						}
					else
						{
						operand = msp430_io(cpu,cpu->reg[reg_num],0,false,(bool)(bw));
						if(cmd_op&8)
							{
							if(bw)
								{
								cpu->reg[reg_num]+=1;
								}
							else
								{
								cpu->reg[reg_num]+=2;
								}
							}
						}
					break;
				}
			}
		break;
		}
	return operand;
	}

uint16_t bcd2bin(uint16_t bcd)
	{
	return (1000*(bcd>>12))+(100*(bcd>>8))+((10 * (bcd >> 4)) + (bcd & 0xf));
	}

uint16_t bin2bcd(uint16_t bin)
	{
	return (
	           ((bin / 10) << 4) +
	           (bin - ((bin / 10) * 10))+
	           (bin - ((bin / 100) * 100))+
	           (bin - ((bin / 1000) * 1000))
	       );
	}

