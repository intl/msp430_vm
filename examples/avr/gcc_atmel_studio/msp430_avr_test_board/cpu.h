#ifndef CPU_H_INCLUDED
#define CPU_H_INCLUDED
#include <stdint.h>

typedef enum { false, true } bool;

///< Указывает использовать ли вызовы (callbacks) вместо отдельных массивов в контексте процессора
///< Использовать ли вызовы для работы с RAM (Вызовы cpu.ram_read, cpu.ram_write)
#define RAM_USE_CALLBACKS 1
///< Использовать ли вызовы для работы с ROM (вызов cpu.rom_read)
#define ROM_USE_CALLBACKS 1
///< Использовать ли вызовы для работы с переферией (вызовы cpu.io_read, cpu.io_write),
///< если 0 то функции работы с переферией должны быть описаны в функции msp430_io
#define IO_USE_CALLBACKS 1

///< Количество ОЗУ
#define RAM_SIZE 128
///< Количество ПЗУ, адреса считаются автоматически, см. ниже разметку адресного пространства
#define ROM_SIZE 1024

///< Указывает будет ли использованы прерывания в проекте. Экономит место.
#define IRQ_USE 1

///< Порядок байт процессора на котором будет запускатся виртуальная машина (Порядок байт хост-машины)
///< AVR и x86 - little endian процессоры
///< STM8 - big endian процесоор

#define BIG_ENDIAN 1
#define LITTLE_ENDIAN 0
#define HOST_ENDIANESS LITTLE_ENDIAN

/********************************************//**
* \brief Директивы отладки
***********************************************/
#define DEBUG_ON 0
#define DEBUG_CPU_MODULE 1
#define DEBUG_IO_MODULE 2
#define DEBUG_INIT_MODULE 4

#define DEBUG_MODULE DEBUG_CPU_MODULE|DEBUG_INIT_MODULE|DEBUG_IO_MODULE


#if(DEBUG_ON)

///< Почему do while : http://stackoverflow.com/questions/1644868/c-define-macro-for-debug-printing

#define debug_print(module,fmt, ...) \
        do {  if(DEBUG_MODULE&&module) fprintf(stderr, fmt, ##__VA_ARGS__); } while (0)
#else
#define debug_print(module,fmt,...) ;
#endif


///< Keil не позволяет использовать анонимные union/struct
///< Что бы это обойти можно использовать ключ компилятора --gnu либо указать через #pragma anon_unions
#if defined ( __CC_ARM )
#pragma anon_unions
#endif


/********************************************//**
 * \brief Разметка адрессного пространства
 *
 * для 1к на 128 байт. Разметка для компилятора (msp430-gcc) указывается в файлах memory.x и periph.x
 * 0x0000 - 0x0003 = SFR
 * В оригинальной версии под SFR отводится 16 байт,
 * но после регистров прерываний, которые занимают 4 байта, ничего нету, так что ограничение в SFR 4 байта
 *
 * 0x0010 - 0x00ff = 8 битная переферия
 * 0x0100 - 0x01ff = 16 Битная переферия
 * 0x0200 - 0x027f = ОЗУ 128 байт. Переменная ram[128] в контексте процессора, либо вызовы ram_read, ram_write
 * 0x0280 - 0xfbff = пустота
 * 0xfc00 - 0xffdf = ПЗУ 1024 байта
 * 0xffe0 - 0xffff = Указатели прерываний (векторы) Main: interrupt vector
 ***********************************************/
#define SFR_START_ADDR 0x0000
#define SFR_END_ADDR 0x0003

#define IO_8BIT_START_ADDR 0x0010
#define IO_8BIT_END_ADDR 0x00ff
#define IO_16BIT_START_ADDR 0x0100
#define IO_16BIT_END_ADDR 0x01ff
#define RAM_START_ADDR 0x200
#define RAM_END_ADDR (RAM_START_ADDR+RAM_SIZE-1)
#define ROM_START_ADDR (ROM_END_ADDR-ROM_SIZE+1)
#define ROM_END_ADDR 0xffff
//#define INT_VECT_START_ADDR 0xffe0
//#define INT_VECT_END_ADDR 0xffff

///< Маски типов команд процессора
//немного через жопу
#define CMD_SINGLE_OP_NUM 1
#define CMD_COND_JUMP_NUM_0 2
#define CMD_COND_JUMP_NUM_1 3
//#define CMD_TWO_OP_ 4-16;

///< однооперандные комаNUMнды, определяются по opcode
#define RRC 0
#define SWPB 1
#define RRA 2
#define SXT 3
#define PUSH 4
#define CALL 5
#define RETI 6

///< двух операндная арифметика, определяются по cmd
#define MOV 4
#define ADD 5
#define ADDC 6
#define SUBC 7
#define SUB 8
#define CMP 9
#define DADD 10
#define BIT_CMD 11
#define BIC 12
#define BIS 13
#define XOR 14
#define AND 15

///< команды условного перехода; PC = PC + 2×offset

#define JNEZ    0       ///< JNE/JNZ
#define JEQZ    1      ///< JEQ/JZ
#define JNCLO   2       ///< JNC/JLO
#define JCHS    3      ///< JC/JHS
#define JN      4      ///< JN
#define JGE     5       ///< JGE
#define JL      6       ///< JL
#define JMP     7      ///< JMP

///< Типы адресаций источника

#define AS_00 0
#define AS_01 1
#define AS_10 2
#define AS_11 3


/********************************************//**
 * \brief вспомогательные функции
 *
 * swap_bytes(x) - перестановка байт в слове
 * constr_16t(msb,lsb) - постройка слова из двух байт
 * constr_opcode(x) постройка opcode из двух половинок
 ***********************************************/
#define swap_bytes(x) ((x>>8)&0xff)|((x<<8)&0xff00)
#define constr_16t(msb,lsb) (msb<<8)|lsb
/********************************************//**
 * \brief Проверки битов регистра статуса
 *
 * check_bw - проверка на байт/слово
 * check_n_bit - проверка на негативный результат
 * check_с_bit - проверка на перенос
 * check_v_bit - переполнение
 * check_z_bit - ноль
 ***********************************************/
#define check_bw(bw,x) ((bw)?(x>>8):x)
#define check_n_bit(operand,bw) (operand>((bw)?0x7f:0x7fff))
#define check_v_bit(src,dst,operand,bw)  (((src<((bw)?0x80:0x8000))&&(dst<((bw)?0x80:0x8000))&&(operand>((bw)?0x7f:0x7fff)))||((src>((bw)?0x7f:0x7fff))&&(dst>((bw)?0x7f:0x7fff))&&(operand<((bw)?0x80:0x8000))))
#define check_z_bit(operand) (operand==0)
#define check_c_bit(dst,operand,bw) (bw)?(operand&0x100):(dst>operand)

/********************************************//**
 * \brief
 *  Структура описывающая команду
 * op -> код команды
 * data -> данные команды
 ***********************************************/
#if(HOST_ENDIANESS)
typedef struct
	{
//[1]
        uint8_t data_msb:4;
	uint8_t op:4;
//[0]
	uint8_t data_lsb:8;
	} msp430_cmd_op_t;
#else
typedef struct
	{
	uint8_t data_lsb:8;
	uint8_t data_msb:4;
	uint8_t op:4;
	} msp430_cmd_op_t;
#endif

/********************************************//**
 * \brief
 * Структкры описывающая однооперадную команду
 * reg - регистр
 * as - адресация источника (src)
 * bw - байт/слово
 * opcode - код команды
 * zeros - нули
 ***********************************************/
#if(HOST_ENDIANESS)
typedef struct
	{
//[1]
        uint8_t opcode_0:2;
        uint8_t :6;
//[0]
	uint8_t reg:4;
        uint8_t as:2;
        uint8_t bw:1;
        uint8_t opcode_1:1;
 	} msp430_cmd_single_op_t;
#else
typedef struct
	{
	uint8_t reg:4;
	uint8_t as:2;
	uint8_t bw:1;
	uint8_t opcode_1:1;
	uint8_t opcode_0:2;
	uint8_t :6;
	} msp430_cmd_single_op_t;
#endif
/********************************************//**
 * \brief Структура описывающее команды перехода
 *
 *  offset - Смещение перехода
 * cond - условие
 ***********************************************/
#if(HOST_ENDIANESS)
typedef struct
	{
//[1]
        uint8_t offset_msb:2;
        uint8_t cond:3; ///< условие
        uint8_t :3;
//[0]
        uint8_t offset_lsb:8; ///< 10 бит смещения относительно PC
	} msp430_cmd_cond_jump_op_t;
#else
typedef struct
	{
	uint8_t offset_lsb:8; ///< 10 бит смещения относительно PC
	uint8_t offset_msb:2;
	uint8_t cond:3; ///< условие
	uint8_t :3;
	} msp430_cmd_cond_jump_op_t;
#endif
/********************************************//**
 * \brief Структура описывающая двух-операндные команды
 *
 * dest - получатель (destination) (в него ложиться результат)
 * as - адресация источника (src)
 * bw - байт/слово
 * ad - адресация получателья (dst)
 * src - источник
 ***********************************************/
#if(HOST_ENDIANESS)
typedef struct
	{
// [1]
        uint8_t src:4;
	uint8_t :4;
//[0]
        uint8_t dest:4;
        uint8_t as:2;
        uint8_t bw:1;
        uint8_t ad:1;
	} msp430_cmd_two_op_t;
#else
typedef struct
	{
	uint8_t dest:4;
	uint8_t as:2;
	uint8_t bw:1;
	uint8_t ad:1;
	uint8_t src:4;
	uint8_t :4;
	} msp430_cmd_two_op_t;
#endif

/********************************************//**
 * \brief Структура описывающая регистр статуса
 ***********************************************/
#if(HOST_ENDIANESS)
typedef struct
	{
	uint8_t :7;
        uint8_t V:1;
        uint8_t SCG1:1;
        uint8_t SCG0:1;
	uint8_t OscOff:1;
	uint8_t CPUOff:1;
	uint8_t GIE:1;
	uint8_t N:1;
	uint8_t Z:1;
        uint8_t C:1;
	} msp430_status_reg_t;
#else
typedef struct
	{
	uint8_t C:1;
	uint8_t Z:1;
	uint8_t N:1;
	uint8_t GIE:1;
	uint8_t CPUOff:1;
	uint8_t OscOff:1;
	uint8_t SCG0:1;
	uint8_t SCG1:1;
	uint8_t V:1;
	uint8_t :7;
	} msp430_status_reg_t;
#endif

#if(IRQ_USE)
/********************************************//**
 * \brief описание прерывания
 *
 * Прерывания подобны описаному в msp430f1x user guide.
 * (Описание оригинальной версии процессора Slau049f (http://www.ti.com/lit/ug/slau049f/slau049f.pdf) пункт 2.2)
 *
 * Но расположение битов сделано по другому. Для удобства.
 * Как и в оригинальных процессорах, есть два регистра
 *
 * Один отвечает за включение прерываний (Interrupt Enable)
 * Если в соответсвующем бите данного регистра установлена 1 то прерывание разрешено.
 * Если прерывание не маскируемое (NMI) его все равно нужно включить, установив соответсвующий бит
 * Только два последних прерывания являются не маскируемыми, но они настраиваются при инициализации переменой msp430_irq_vectors битами NMI
 * Маскировка прерывания выполняется лишь битом GIE ( int -> gie|nmi)
 *
 * Второй регистр ответчает за возникновения события прерывания.
 * Если в соответствующем бите будет установлена 1 и прерывание разрешено, то прерывание будет выполнено
 *
 ***********************************************/


typedef struct
	{
	///< Регистры Special function register
	union
		{
        uint8_t SFR[4];
		struct
			{
			union
				{
				///< Регистр разрешения прерывания, адресс 0-1 (16бит)
				uint16_t IE_REG;
				struct
					{
					uint8_t IRQ0_IE:1;
					uint8_t IRQ1_IE:1;
					uint8_t IRQ2_IE:1;
					uint8_t IRQ3_IE:1;
					uint8_t IRQ4_IE:1;
					uint8_t IRQ5_IE:1;
					uint8_t IRQ6_IE:1;
					uint8_t IRQ7_IE:1;
					uint8_t IRQ8_IE:1;
					uint8_t IRQ9_IE:1;
					uint8_t IRQ10_IE:1;
					uint8_t IRQ11_IE:1;
					uint8_t IRQ12_IE:1;
					uint8_t IRQ13_IE:1;
					///< IO fault error (flash access violation int ena)
					///< В оригинальной версии ACCVIE
					uint8_t IO_ERROR_IE:1;
					uint8_t RST_IE:1;
					} IE_REG_bits;
				};
			union
				{
				///< Решистр вызова прерывания, адресс 2-3 (16бит)
				uint16_t IF_REG;
				struct
					{
					uint8_t IRQ0_IFG:1;
					uint8_t IRQ1_IFG:1;
					uint8_t IRQ2_IFG:1;
					uint8_t IRQ3_IFG:1;
					uint8_t IRQ4_IFG:1;
					uint8_t IRQ5_IFG:1;
					uint8_t IRQ6_IFG:1;
					uint8_t IRQ7_IFG:1;
					uint8_t IRQ8_IFG:1;
					uint8_t IRQ9_IFG:1;
					uint8_t IRQ10_IFG:1;
					uint8_t IRQ11_IFG:1;
					uint8_t IRQ12_IFG:1;
					uint8_t IRQ13_IFG:1;
					///< IO fault error (flash access violation int flag)
					///< В оригинальной версии ACCVIFG
					uint8_t IO_ERROR_IFG:1;
					uint8_t RST_IFG:1;
					} IF_REG_bits;
				};
			};
		};
	}
msp430_irq_t;

typedef struct
	{
	uint16_t vect;
	uint8_t nmi:1;
	uint8_t :7;
	} msp430_irq_vector_t;
#endif


/********************************************//**
 * \brief Контекст процессора (набор со всеми регистрами и памятью)
 ***********************************************/

typedef struct
	{
	union
		{
		uint16_t reg[16];
		struct
			{
			uint16_t PC_REG;
			uint16_t SP_REG;
			union
				{
				msp430_status_reg_t SR_REG_bits;
				uint16_t SR_REG;
				uint16_t GC1_REG;
				};
			union
				{
				uint16_t R3;
				uint16_t GC2_REG;
				};
			uint16_t R4;
			uint16_t R5;
			uint16_t R6;
			uint16_t R7;
			uint16_t R8;
			uint16_t R9;
			uint16_t R10;
			uint16_t R11;
			uint16_t R12;
			uint16_t R13;
			uint16_t R14;
			uint16_t R15;
			};
		};

    #if(IRQ_USE)
	msp430_irq_t irq;
    #endif


	#if(RAM_USE_CALLBACKS)
	///< callbacks для ОЗУ
	uint8_t (*ram_read_cb)(uint16_t addr);
	void (*ram_write_cb)(uint16_t addr,uint8_t data);
	#else
	uint8_t ram[RAM_SIZE];
	#endif

	#if(ROM_USE_CALLBACKS)
	///< callbacks для ПЗУ
	uint8_t (*rom_read_cb)(uint16_t addr);
	#else
	uint8_t rom[ROM_SIZE];
	#endif

    #if(IO_USE_CALLBACKS)
	///< callbacks для ПЗУ
	uint16_t (*io_read_cb)(uint16_t addr);
	void (*io_write_cb)(uint16_t addr,uint16_t data);
	#endif

	uint16_t cmd;
	} msp430_context_t;

#if(IRQ_USE)
///> Функция для вызовы прерывания в виртуальном процессоре
void msp430_interrupt(msp430_context_t* cpu,uint8_t num);
#endif

///> Функция инициализации процессороа
void msp430_init(msp430_context_t* cpu);
///> Функция обработки команды процессора
void msp430_cpu(msp430_context_t* cpu);
///> Функция ввода/вывода процессора
uint16_t msp430_io(msp430_context_t* cpu,uint16_t addr, uint16_t data ,bool write,bool bw);
///> Функция адрессация источника (src)
uint16_t msp430_addr_src_mode(msp430_context_t* cpu,uint8_t as, uint8_t reg_num,uint16_t operand,bool write,uint8_t bw,uint8_t cmd_op);
///> Функция адрессации приемника (dst)
uint16_t msp430_addr_dst_mode(msp430_context_t* cpu,uint8_t ad,uint8_t reg_num,uint16_t data, bool write, bool bw);

uint16_t bcd2bin(uint16_t bcd);
uint16_t bin2bcd(uint16_t bin);
#endif // CPU_H_INCLUDED
