/**********************************************************

Software I2C Library for AVR Devices.

Copyright 2008-2012
eXtreme Electronics, India
www.eXtremeElectronics.co.in
 **********************************************************/
#include <stdint.h>
#include <iostm8s003f3.h>
#include "i2csoft.h"

#define Nop() asm("Nop");
#define Q_DEL Nop(); Nop(); Nop();Nop();
#define H_DEL Nop(); Nop(); Nop(); Nop(); Nop(); Nop();

void
SoftI2CInit ()
{
  SDADDR |= (1 << SDA);
  SCLDDR |= (1 << SCL);

  SOFT_I2C_SDA_HIGH;
  SOFT_I2C_SCL_HIGH;
}

void SoftI2CStart ()
{
  SOFT_I2C_SCL_HIGH;
  H_DEL;
  SOFT_I2C_SDA_LOW;
  H_DEL;
}

void SoftI2CStop ()
{
  SOFT_I2C_SDA_LOW;
  H_DEL;
  SOFT_I2C_SCL_HIGH;
  Q_DEL;
  SOFT_I2C_SDA_HIGH;
  H_DEL;
}

uint8_t SoftI2CWriteByte (uint8_t data)
{
  uint8_t i,ack = 0;
  for (i = 0; i < 8; i++)
    {
      SOFT_I2C_SCL_LOW;
      Q_DEL;
      if (data & 0x80)
        SOFT_I2C_SDA_HIGH;
      else
        SOFT_I2C_SDA_LOW;
      H_DEL;
      SOFT_I2C_SCL_HIGH;
      H_DEL;
      data = data << 1;
    }
  //The 9th clock (ACK Phase)
  SOFT_I2C_SCL_LOW;
  Q_DEL;
  SOFT_I2C_SDA_HIGH;
  H_DEL;
  SOFT_I2C_SCL_HIGH;
  H_DEL;
  ack = (SDAPIN & (1 << SDA));
  SOFT_I2C_SCL_LOW;
  H_DEL;
  return ack;
}

uint8_t SoftI2CReadByte (uint8_t ack)
{
  uint8_t data = 0x00;
  uint8_t i;

  SDADDR &= ~(1<<SDA);
  
  for (i = 0; i < 8; i++)
    {

      SOFT_I2C_SCL_LOW;
      H_DEL;
     
       if (SDAPIN & (1 << SDA))
        data |= (0x80 >> i);

       SOFT_I2C_SCL_HIGH;
      H_DEL;
    }
  
  SDADDR |= (1<<SDA);
  SOFT_I2C_SCL_LOW;
  Q_DEL; //Soft_I2C_Put_Ack
  if (ack)
    {
      SOFT_I2C_SDA_LOW;
    }
  else
    {
      SOFT_I2C_SDA_HIGH;
    }
  H_DEL;

  SOFT_I2C_SCL_HIGH;
  H_DEL;

  SOFT_I2C_SCL_LOW;
  H_DEL;

  SOFT_I2C_SDA_HIGH;
  
  return data;
}















