#include "stm8.h"
#include "cpu.h"
#include "i2csoft.h"


///< ���������� ���
uint8_t ram[128] = { 0 };
	
///< ���������� ���
/*
���� ������ �� code composer studio 6.0.1 - blink.c
������ �� TI �����������

���������� ����� blink.c:

#include <msp430.h>

int main(void) {
	__bis_SR_register( LPM0_bits);
	P1DIR |= (1<<5);					// Set P1.0 to output direction

	for(;;) {
		volatile unsigned int i;	// volatile to prevent optimization
		P1OUT ^= (1<<5);				// Toggle P1.0 using exclusive-OR
		i = 10000;					// SW Delay
		do i--;
		while(i != 0);
	}

}
*/
#define SLAVE_ADDRESS 0xA0

///< ������� �������������� (callback) ��������� ������������ ��� ������ � �������� 8/16 ������ ���������
///< ������ ����� ����������� ��������� ����������
void io_write(uint16_t addr, uint16_t data)
{
	///< ����� ����������� ��������� ����������, ��� ����� �� �������� AVR PORTB �� P1 MSP430_VM
	switch ( addr)
	{
		///< ������� P1IN
		case 0x20:
			PC_IDR = data; 
		 break;
		///< ������� P1OUT � MSP430F1101 ��������� �� ������ 0x21. �������� �� ������ � ����1 (���������� PORT � AVR)
		case 0x21:
			PC_ODR = data;
		break;
		///< �������� P1DIR � MSP430f1101 ��������� �� ������� 0x20. �������� �� ����������� ������ ����1 (���������� DDR)
		case 0x22:
			PC_DDR = data;
		break;
		default: break;
	}

}

///< ������� �������������� (callback) ��������� ������������ ��� ������ �� ��������� 8/16 ������ ��������� 
uint16_t io_read(uint16_t addr)
{	
	switch ( addr)
	{
		///< ������� P1IN
		case 0x20:
		return PC_IDR;
		///< ������� P1OUT � MSP430F1101 ��������� �� ������ 0x21. �������� �� ������ � ����1 (���������� PORT � AVR)
		case 0x21:
		return PC_ODR;
		///< �������� P1DIR � MSP430f1101 ��������� �� ������� 0x20. �������� �� ����������� ������ ����1 (���������� DDR)
		case 0x22:
		return PC_DDR;
		default: 
		return 0;
		break;
	}
}

///< �������������� ��� ������ � RAM 
///< ���� �� �� ������ ������������ �������������� ���������� RAM_USE_CALLBACK 0, ����� ����� �������� ������ ��� � ��������� ���������� (cpu->ram[RAM_SIZE]) 
uint8_t ram_read(uint16_t addr)
{
	return ram[addr];
}

///< �������������� ��� ������ � RAM
void ram_write(uint16_t addr, uint8_t data)
{
	ram[addr]=data;
}

///< �������������� ��� ������ �� ROM
uint8_t rom_read(uint16_t address)
{
  uint8_t data = 0;
  SoftI2CStart ();
  if (SoftI2CWriteByte (SLAVE_ADDRESS)) return 0;
  if (SoftI2CWriteByte ((address>>8))) return 0;
  if (SoftI2CWriteByte ((address&0xff))) return 0;
  SoftI2CStart ();
  if (SoftI2CWriteByte (SLAVE_ADDRESS | 0x01)) return 0;
  data = SoftI2CReadByte (0);
  SoftI2CStop ();
  return data;
}


static void sys_cfg(void)
{
  CLK_PCKENR1 = 0;
  CLK_PCKENR2 = 0;
  CLK_DIVR = 0;
  CLK_ICKR |= CLK_ICKR_HSIEN;
  while(CLK_ICKR&CLK_ICKR_HSIRDY);
  CLK_SWR = 0xE1;
}


int main(void)
{
	///< ��������� �������������� (callbacks) ��� ������ � �������� ������������� ���������� 
	///< SDCC ������� ��� �� ��� ��������� ���������� ���� ��������� � ������ �������
	msp430_context_t cpu_context =
	{
		.ram_read_cb = ram_read,
		.ram_write_cb = ram_write,
		.rom_read_cb = rom_read,
		.io_read_cb = io_read,
		.io_write_cb = io_write
	};
	sys_cfg();
	///<������������� ��������� I2C
	SoftI2CInit();
	PC_CR1 = 0xff;
	
	///< ������������� ���������� (��������� �������� ������ �� ������� 15-�� ���������� () ) 
	msp430_init(&cpu_context);
	
	while(1)
	{
		///< ���������� ������� �� ��������� cpu
		msp430_cpu(&cpu_context);
	}
	
}