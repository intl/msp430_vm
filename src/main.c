#include <stdio.h>
#include <stdlib.h>
#include "cpu.h"

uint8_t ram[128] = {};
uint8_t rom[1024] = {};

void fill_rom(uint8_t* rom)
	{
	FILE * pFile;
	pFile=fopen ("memory.bin","rb");
	if (pFile==NULL) perror ("Error opening file");
	else
		{
		for(uint16_t addr = 0; addr!=ROM_SIZE; addr++)
			{
			rom[addr] = fgetc(pFile);
			}
		fclose (pFile);
		}
	}

uint8_t ram_read(uint16_t addr)
	{
	return ram[addr];
	}

void ram_write(uint16_t addr, uint8_t data)
	{
	ram[addr]=data;
	}

uint8_t rom_read(uint16_t addr)
	{
	return rom[addr];
	}

uint16_t io_read(uint16_t addr)
	{
	return 0;
	}

void io_write(uint16_t addr, uint16_t data)
	{

	}

int main()
	{
	fill_rom(rom);

	msp430_context_t cpu_context =
		{
		.ram_read_cb = ram_read,
		.ram_write_cb = ram_write,
		.rom_read_cb = rom_read,
		.io_read_cb = io_read,
		.io_write_cb = io_write,
		};


	msp430_init(&cpu_context);
	while(1)
		{
		msp430_cpu(&cpu_context);
		}

	return 0;
	}

