
main.o:     file format elf32-msp430


Disassembly of section .text:

0000fc00 <__watchdog_support>:
    fc00:	55 42 20 01 	mov.b	&0x0120,r5	
    fc04:	35 d0 08 5a 	bis	#23048,	r5	;#0x5a08
    fc08:	82 45 00 02 	mov	r5,	&0x0200	

0000fc0c <__init_stack>:
    fc0c:	31 40 80 02 	mov	#640,	r1	;#0x0280

0000fc10 <__do_copy_data>:
    fc10:	3f 40 00 00 	mov	#0,	r15	;#0x0000
    fc14:	0f 93       	tst	r15		
    fc16:	08 24       	jz	$+18     	;abs 0xfc28
    fc18:	92 42 00 02 	mov	&0x0200,&0x0120	
    fc1c:	20 01 
    fc1e:	2f 83       	decd	r15		
    fc20:	9f 4f dc fc 	mov	-804(r15),512(r15);0xfcdc(r15), 0x0200(r15)
    fc24:	00 02 
    fc26:	f8 23       	jnz	$-14     	;abs 0xfc18

0000fc28 <__do_clear_bss>:
    fc28:	3f 40 00 00 	mov	#0,	r15	;#0x0000
    fc2c:	0f 93       	tst	r15		
    fc2e:	07 24       	jz	$+16     	;abs 0xfc3e
    fc30:	92 42 00 02 	mov	&0x0200,&0x0120	
    fc34:	20 01 
    fc36:	1f 83       	dec	r15		
    fc38:	cf 43 00 02 	mov.b	#0,	512(r15);r3 As==00, 0x0200(r15)
    fc3c:	f9 23       	jnz	$-12     	;abs 0xfc30

0000fc3e <main>:
    fc3e:	04 41       	mov	r1,	r4	
    fc40:	24 53       	incd	r4		
    fc42:	21 83       	decd	r1		
    fc44:	b0 12 8a fc 	call	#0xfc8a	
    fc48:	c4 43 fc ff 	mov.b	#0,	-4(r4)	;r3 As==00, 0xfffc(r4)
    fc4c:	32 d2       	eint			
    fc4e:	d4 53 fc ff 	inc.b	-4(r4)		;0xfffc(r4)
    fc52:	d2 44 fc ff 	mov.b	-4(r4),	&0x0021	;0xfffc(r4)
    fc56:	21 00 
    fc58:	fa 3f       	jmp	$-10     	;abs 0xfc4e

0000fc5a <__stop_progExec__>:
    fc5a:	32 d0 f0 00 	bis	#240,	r2	;#0x00f0
    fc5e:	fd 3f       	jmp	$-4      	;abs 0xfc5a

0000fc60 <__ctors_end>:
    fc60:	30 40 da fc 	br	#0xfcda	

0000fc64 <test_irq_13>:
    fc64:	0f 12       	push	r15		
    fc66:	04 12       	push	r4		
    fc68:	04 41       	mov	r1,	r4	
    fc6a:	24 52       	add	#4,	r4	;r2 As==10
    fc6c:	5f 42 21 00 	mov.b	&0x0021,r15	
    fc70:	7f e0 40 00 	xor.b	#64,	r15	;#0x0040
    fc74:	c2 4f 21 00 	mov.b	r15,	&0x0021	
    fc78:	5f 42 23 00 	mov.b	&0x0023,r15	
    fc7c:	7f f0 f7 ff 	and.b	#-9,	r15	;#0xfff7
    fc80:	c2 4f 23 00 	mov.b	r15,	&0x0023	
    fc84:	34 41       	pop	r4		
    fc86:	3f 41       	pop	r15		
    fc88:	00 13       	reti			

0000fc8a <p1_init>:
    fc8a:	04 12       	push	r4		
    fc8c:	04 41       	mov	r1,	r4	
    fc8e:	24 53       	incd	r4		
    fc90:	5f 42 21 00 	mov.b	&0x0021,r15	
    fc94:	c2 43 21 00 	mov.b	#0,	&0x0021	;r3 As==00
    fc98:	5f 42 22 00 	mov.b	&0x0022,r15	
    fc9c:	c2 43 22 00 	mov.b	#0,	&0x0022	;r3 As==00
    fca0:	5f 42 22 00 	mov.b	&0x0022,r15	
    fca4:	7f d0 41 00 	bis.b	#65,	r15	;#0x0041
    fca8:	c2 4f 22 00 	mov.b	r15,	&0x0022	
    fcac:	5f 42 21 00 	mov.b	&0x0021,r15	
    fcb0:	7f d2       	bis.b	#8,	r15	;r2 As==11
    fcb2:	c2 4f 21 00 	mov.b	r15,	&0x0021	
    fcb6:	5f 42 25 00 	mov.b	&0x0025,r15	
    fcba:	7f d2       	bis.b	#8,	r15	;r2 As==11
    fcbc:	c2 4f 25 00 	mov.b	r15,	&0x0025	
    fcc0:	5f 42 24 00 	mov.b	&0x0024,r15	
    fcc4:	7f d2       	bis.b	#8,	r15	;r2 As==11
    fcc6:	c2 4f 24 00 	mov.b	r15,	&0x0024	
    fcca:	5f 42 23 00 	mov.b	&0x0023,r15	
    fcce:	7f f0 f7 ff 	and.b	#-9,	r15	;#0xfff7
    fcd2:	c2 4f 23 00 	mov.b	r15,	&0x0023	
    fcd6:	34 41       	pop	r4		
    fcd8:	30 41       	ret			

0000fcda <_unexpected_>:
    fcda:	00 13       	reti			

Disassembly of section .noinit:

00000200 <__wdt_clear_value>:
	...

Disassembly of section .vectors:

0000ffe0 <__ivtbl_16>:
    ffe0:	64 fc       	and.b	@r12,	r4	
    ffe2:	60 fc       	and.b	@r12,	r0	
    ffe4:	60 fc       	and.b	@r12,	r0	
    ffe6:	60 fc       	and.b	@r12,	r0	
    ffe8:	60 fc       	and.b	@r12,	r0	
    ffea:	60 fc       	and.b	@r12,	r0	
    ffec:	60 fc       	and.b	@r12,	r0	
    ffee:	60 fc       	and.b	@r12,	r0	
    fff0:	60 fc       	and.b	@r12,	r0	
    fff2:	60 fc       	and.b	@r12,	r0	
    fff4:	60 fc       	and.b	@r12,	r0	
    fff6:	60 fc       	and.b	@r12,	r0	
    fff8:	60 fc       	and.b	@r12,	r0	
    fffa:	60 fc       	and.b	@r12,	r0	
    fffc:	60 fc       	and.b	@r12,	r0	
    fffe:	00 fc       	and	r12,	r0	

Disassembly of section .debug_aranges:

00000000 <.debug_aranges>:
   0:	10 00       	.word	0x0010;	????	
   2:	00 00       	.word	0x0000;	????	
   4:	02 00       	.word	0x0002;	????	
   6:	00 00       	.word	0x0000;	????	
   8:	00 00       	.word	0x0000;	????	
   a:	02 00       	.word	0x0002;	????	
   c:	00 fc       	and	r12,	r0	
   e:	0c 00       	.word	0x000c;	????	
  10:	00 00       	.word	0x0000;	????	
  12:	00 00       	.word	0x0000;	????	
  14:	10 00       	.word	0x0010;	????	
  16:	00 00       	.word	0x0000;	????	
  18:	02 00       	.word	0x0002;	????	
  1a:	b8 00       	.word	0x00b8;	????	
  1c:	00 00       	.word	0x0000;	????	
  1e:	02 00       	.word	0x0002;	????	
  20:	0c fc       	and	r12,	r12	
  22:	04 00       	.word	0x0004;	????	
  24:	00 00       	.word	0x0000;	????	
  26:	00 00       	.word	0x0000;	????	
  28:	10 00       	.word	0x0010;	????	
  2a:	00 00       	.word	0x0000;	????	
  2c:	02 00       	.word	0x0002;	????	
  2e:	70 01       	.word	0x0170;	????	
  30:	00 00       	.word	0x0000;	????	
  32:	02 00       	.word	0x0002;	????	
  34:	10 fc 18 00 	and	24(r12),r0	;0x0018(r12)
  38:	00 00       	.word	0x0000;	????	
  3a:	00 00       	.word	0x0000;	????	
  3c:	10 00       	.word	0x0010;	????	
  3e:	00 00       	.word	0x0000;	????	
  40:	02 00       	.word	0x0002;	????	
  42:	28 02       	.word	0x0228;	????	
  44:	00 00       	.word	0x0000;	????	
  46:	02 00       	.word	0x0002;	????	
  48:	28 fc       	and	@r12,	r8	
  4a:	16 00       	.word	0x0016;	????	
  4c:	00 00       	.word	0x0000;	????	
  4e:	00 00       	.word	0x0000;	????	
  50:	10 00       	.word	0x0010;	????	
  52:	00 00       	.word	0x0000;	????	
  54:	02 00       	.word	0x0002;	????	
  56:	e0 02       	.word	0x02e0;	????	
  58:	00 00       	.word	0x0000;	????	
  5a:	02 00       	.word	0x0002;	????	
  5c:	5a fc 06 00 	and.b	6(r12),	r10	;0x0006(r12)
  60:	00 00       	.word	0x0000;	????	
  62:	00 00       	.word	0x0000;	????	
  64:	10 00       	.word	0x0010;	????	
  66:	00 00       	.word	0x0000;	????	
  68:	02 00       	.word	0x0002;	????	
  6a:	98 03       	.word	0x0398;	????	
  6c:	00 00       	.word	0x0000;	????	
  6e:	02 00       	.word	0x0002;	????	
  70:	da fc 02 00 	and.b	2(r12),	0(r10)	;0x0002(r12), 0x0000(r10)
  74:	00 00 
	...

Disassembly of section .debug_info:

00000000 <.debug_info>:
   0:	b4 00       	.word	0x00b4;	????	
   2:	00 00       	.word	0x0000;	????	
   4:	02 00       	.word	0x0002;	????	
   6:	00 00       	.word	0x0000;	????	
   8:	00 00       	.word	0x0000;	????	
   a:	02 01       	.word	0x0102;	????	
   c:	00 00       	.word	0x0000;	????	
   e:	00 00       	.word	0x0000;	????	
  10:	00 fc       	and	r12,	r0	
  12:	0c fc       	and	r12,	r12	
  14:	2f 62       	addc	#4,	r15	;r2 As==10
  16:	75 69       	addc.b	@r9+,	r5	
  18:	6c 64       	addc.b	@r4,	r12	
  1a:	2f 62       	addc	#4,	r15	;r2 As==10
  1c:	75 69       	addc.b	@r9+,	r5	
  1e:	6c 64       	addc.b	@r4,	r12	
  20:	64 2f       	jc	$-310    	;abs 0xfeea
  22:	67 63       	addc.b	#2,	r7	;r3 As==10
  24:	63 2d       	jc	$+712    	;abs 0x2ec
  26:	6d 73       	subc.b	#2,	r13	;r3 As==10
  28:	70 34       	jge	$+226    	;abs 0x10a
  2a:	33 30       	jn	$+104    	;abs 0x92
  2c:	2d 34       	jge	$+92     	;abs 0x88
  2e:	2e 36       	jge	$-930    	;abs 0xfc8c
  30:	2e 33       	jn	$-418    	;abs 0xfe8e
  32:	7e 6d       	addc.b	@r13+,	r14	
  34:	73 70       	.word	0x7073;	????	Illegal as 2-op instr
  36:	67 63       	addc.b	#2,	r7	;r3 As==10
  38:	63 2d       	jc	$+712    	;abs 0x300
  3a:	32 30       	jn	$+102    	;abs 0xa0
  3c:	31 32       	jn	$-924    	;abs 0xfca0
  3e:	30 34       	jge	$+98     	;abs 0xa0
  40:	30 36       	jge	$-926    	;abs 0xfca2
  42:	2f 2e       	jc	$-928    	;abs 0xfca2
  44:	2f 67       	addc	@r7,	r15	
  46:	63 63       	.word	0x6363;	????	Illegal as 2-op instr
  48:	2d 34       	jge	$+92     	;abs 0xa4
  4a:	2e 36       	jge	$-930    	;abs 0xfca8
  4c:	2e 33       	jn	$-418    	;abs 0xfeaa
  4e:	2f 67       	addc	@r7,	r15	
  50:	63 63       	.word	0x6363;	????	Illegal as 2-op instr
  52:	2f 63       	addc	#2,	r15	;r3 As==10
  54:	6f 6e       	addc.b	@r14,	r15	
  56:	66 69       	addc.b	@r9,	r6	
  58:	67 2f       	jc	$-304    	;abs 0xff28
  5a:	6d 73       	subc.b	#2,	r13	;r3 As==10
  5c:	70 34       	jge	$+226    	;abs 0x13e
  5e:	33 30       	jn	$+104    	;abs 0xc6
  60:	2f 63       	addc	#2,	r15	;r3 As==10
  62:	72 74       	subc.b	@r4+,	r2	
  64:	30 2e       	jc	$-926    	;abs 0xfcc6
  66:	53 00       	.word	0x0053;	????	
  68:	2f 62       	addc	#4,	r15	;r2 As==10
  6a:	75 69       	addc.b	@r9+,	r5	
  6c:	6c 64       	addc.b	@r4,	r12	
  6e:	2f 62       	addc	#4,	r15	;r2 As==10
  70:	75 69       	addc.b	@r9+,	r5	
  72:	6c 64       	addc.b	@r4,	r12	
  74:	64 2f       	jc	$-310    	;abs 0xff3e
  76:	67 63       	addc.b	#2,	r7	;r3 As==10
  78:	63 2d       	jc	$+712    	;abs 0x340
  7a:	6d 73       	subc.b	#2,	r13	;r3 As==10
  7c:	70 34       	jge	$+226    	;abs 0x15e
  7e:	33 30       	jn	$+104    	;abs 0xe6
  80:	2d 34       	jge	$+92     	;abs 0xdc
  82:	2e 36       	jge	$-930    	;abs 0xfce0
  84:	2e 33       	jn	$-418    	;abs 0xfee2
  86:	7e 6d       	addc.b	@r13+,	r14	
  88:	73 70       	.word	0x7073;	????	Illegal as 2-op instr
  8a:	67 63       	addc.b	#2,	r7	;r3 As==10
  8c:	63 2d       	jc	$+712    	;abs 0x354
  8e:	32 30       	jn	$+102    	;abs 0xf4
  90:	31 32       	jn	$-924    	;abs 0xfcf4
  92:	30 34       	jge	$+98     	;abs 0xf4
  94:	30 36       	jge	$-926    	;abs 0xfcf6
  96:	2f 62       	addc	#4,	r15	;r2 As==10
  98:	75 69       	addc.b	@r9+,	r5	
  9a:	6c 64       	addc.b	@r4,	r12	
  9c:	2d 72       	subc	#4,	r13	;r2 As==10
  9e:	65 73       	subc.b	#2,	r5	;r3 As==10
  a0:	75 6c       	addc.b	@r12+,	r5	
  a2:	74 2f       	jc	$-278    	;abs 0xff8c
  a4:	67 63       	addc.b	#2,	r7	;r3 As==10
  a6:	63 00       	.word	0x0063;	????	
  a8:	47 4e       	mov.b	r14,	r7	
  aa:	55 20       	jnz	$+172    	;abs 0x156
  ac:	41 53       	add.b	#0,	r1	;r3 As==00
  ae:	20 32       	jn	$-958    	;abs 0xfcf0
  b0:	2e 32       	jn	$-930    	;abs 0xfd0e
  b2:	31 2e       	jc	$-924    	;abs 0xfd16
  b4:	31 00       	.word	0x0031;	????	
  b6:	01 80       	sub	r0,	r1	
  b8:	b4 00       	.word	0x00b4;	????	
  ba:	00 00       	.word	0x0000;	????	
  bc:	02 00       	.word	0x0002;	????	
  be:	14 00       	.word	0x0014;	????	
  c0:	00 00       	.word	0x0000;	????	
  c2:	02 01       	.word	0x0102;	????	
  c4:	84 00       	.word	0x0084;	????	
  c6:	00 00       	.word	0x0000;	????	
  c8:	0c fc       	and	r12,	r12	
  ca:	10 fc 2f 62 	and	25135(r12),r0	;0x622f(r12)
  ce:	75 69       	addc.b	@r9+,	r5	
  d0:	6c 64       	addc.b	@r4,	r12	
  d2:	2f 62       	addc	#4,	r15	;r2 As==10
  d4:	75 69       	addc.b	@r9+,	r5	
  d6:	6c 64       	addc.b	@r4,	r12	
  d8:	64 2f       	jc	$-310    	;abs 0xffa2
  da:	67 63       	addc.b	#2,	r7	;r3 As==10
  dc:	63 2d       	jc	$+712    	;abs 0x3a4
  de:	6d 73       	subc.b	#2,	r13	;r3 As==10
  e0:	70 34       	jge	$+226    	;abs 0x1c2
  e2:	33 30       	jn	$+104    	;abs 0x14a
  e4:	2d 34       	jge	$+92     	;abs 0x140
  e6:	2e 36       	jge	$-930    	;abs 0xfd44
  e8:	2e 33       	jn	$-418    	;abs 0xff46
  ea:	7e 6d       	addc.b	@r13+,	r14	
  ec:	73 70       	.word	0x7073;	????	Illegal as 2-op instr
  ee:	67 63       	addc.b	#2,	r7	;r3 As==10
  f0:	63 2d       	jc	$+712    	;abs 0x3b8
  f2:	32 30       	jn	$+102    	;abs 0x158
  f4:	31 32       	jn	$-924    	;abs 0xfd58
  f6:	30 34       	jge	$+98     	;abs 0x158
  f8:	30 36       	jge	$-926    	;abs 0xfd5a
  fa:	2f 2e       	jc	$-928    	;abs 0xfd5a
  fc:	2f 67       	addc	@r7,	r15	
  fe:	63 63       	.word	0x6363;	????	Illegal as 2-op instr
 100:	2d 34       	jge	$+92     	;abs 0x15c
 102:	2e 36       	jge	$-930    	;abs 0xfd60
 104:	2e 33       	jn	$-418    	;abs 0xff62
 106:	2f 67       	addc	@r7,	r15	
 108:	63 63       	.word	0x6363;	????	Illegal as 2-op instr
 10a:	2f 63       	addc	#2,	r15	;r3 As==10
 10c:	6f 6e       	addc.b	@r14,	r15	
 10e:	66 69       	addc.b	@r9,	r6	
 110:	67 2f       	jc	$-304    	;abs 0xffe0
 112:	6d 73       	subc.b	#2,	r13	;r3 As==10
 114:	70 34       	jge	$+226    	;abs 0x1f6
 116:	33 30       	jn	$+104    	;abs 0x17e
 118:	2f 63       	addc	#2,	r15	;r3 As==10
 11a:	72 74       	subc.b	@r4+,	r2	
 11c:	30 2e       	jc	$-926    	;abs 0xfd7e
 11e:	53 00       	.word	0x0053;	????	
 120:	2f 62       	addc	#4,	r15	;r2 As==10
 122:	75 69       	addc.b	@r9+,	r5	
 124:	6c 64       	addc.b	@r4,	r12	
 126:	2f 62       	addc	#4,	r15	;r2 As==10
 128:	75 69       	addc.b	@r9+,	r5	
 12a:	6c 64       	addc.b	@r4,	r12	
 12c:	64 2f       	jc	$-310    	;abs 0xfff6
 12e:	67 63       	addc.b	#2,	r7	;r3 As==10
 130:	63 2d       	jc	$+712    	;abs 0x3f8
 132:	6d 73       	subc.b	#2,	r13	;r3 As==10
 134:	70 34       	jge	$+226    	;abs 0x216
 136:	33 30       	jn	$+104    	;abs 0x19e
 138:	2d 34       	jge	$+92     	;abs 0x194
 13a:	2e 36       	jge	$-930    	;abs 0xfd98
 13c:	2e 33       	jn	$-418    	;abs 0xff9a
 13e:	7e 6d       	addc.b	@r13+,	r14	
 140:	73 70       	.word	0x7073;	????	Illegal as 2-op instr
 142:	67 63       	addc.b	#2,	r7	;r3 As==10
 144:	63 2d       	jc	$+712    	;abs 0x40c
 146:	32 30       	jn	$+102    	;abs 0x1ac
 148:	31 32       	jn	$-924    	;abs 0xfdac
 14a:	30 34       	jge	$+98     	;abs 0x1ac
 14c:	30 36       	jge	$-926    	;abs 0xfdae
 14e:	2f 62       	addc	#4,	r15	;r2 As==10
 150:	75 69       	addc.b	@r9+,	r5	
 152:	6c 64       	addc.b	@r4,	r12	
 154:	2d 72       	subc	#4,	r13	;r2 As==10
 156:	65 73       	subc.b	#2,	r5	;r3 As==10
 158:	75 6c       	addc.b	@r12+,	r5	
 15a:	74 2f       	jc	$-278    	;abs 0x44
 15c:	67 63       	addc.b	#2,	r7	;r3 As==10
 15e:	63 00       	.word	0x0063;	????	
 160:	47 4e       	mov.b	r14,	r7	
 162:	55 20       	jnz	$+172    	;abs 0x20e
 164:	41 53       	add.b	#0,	r1	;r3 As==00
 166:	20 32       	jn	$-958    	;abs 0xfda8
 168:	2e 32       	jn	$-930    	;abs 0xfdc6
 16a:	31 2e       	jc	$-924    	;abs 0xfdce
 16c:	31 00       	.word	0x0031;	????	
 16e:	01 80       	sub	r0,	r1	
 170:	b4 00       	.word	0x00b4;	????	
 172:	00 00       	.word	0x0000;	????	
 174:	02 00       	.word	0x0002;	????	
 176:	28 00       	.word	0x0028;	????	
 178:	00 00       	.word	0x0000;	????	
 17a:	02 01       	.word	0x0102;	????	
 17c:	06 01       	.word	0x0106;	????	
 17e:	00 00       	.word	0x0000;	????	
 180:	10 fc 28 fc 	and	-984(r12),r0	;0xfc28(r12)
 184:	2f 62       	addc	#4,	r15	;r2 As==10
 186:	75 69       	addc.b	@r9+,	r5	
 188:	6c 64       	addc.b	@r4,	r12	
 18a:	2f 62       	addc	#4,	r15	;r2 As==10
 18c:	75 69       	addc.b	@r9+,	r5	
 18e:	6c 64       	addc.b	@r4,	r12	
 190:	64 2f       	jc	$-310    	;abs 0x5a
 192:	67 63       	addc.b	#2,	r7	;r3 As==10
 194:	63 2d       	jc	$+712    	;abs 0x45c
 196:	6d 73       	subc.b	#2,	r13	;r3 As==10
 198:	70 34       	jge	$+226    	;abs 0x27a
 19a:	33 30       	jn	$+104    	;abs 0x202
 19c:	2d 34       	jge	$+92     	;abs 0x1f8
 19e:	2e 36       	jge	$-930    	;abs 0xfdfc
 1a0:	2e 33       	jn	$-418    	;abs 0xfffe
 1a2:	7e 6d       	addc.b	@r13+,	r14	
 1a4:	73 70       	.word	0x7073;	????	Illegal as 2-op instr
 1a6:	67 63       	addc.b	#2,	r7	;r3 As==10
 1a8:	63 2d       	jc	$+712    	;abs 0x470
 1aa:	32 30       	jn	$+102    	;abs 0x210
 1ac:	31 32       	jn	$-924    	;abs 0xfe10
 1ae:	30 34       	jge	$+98     	;abs 0x210
 1b0:	30 36       	jge	$-926    	;abs 0xfe12
 1b2:	2f 2e       	jc	$-928    	;abs 0xfe12
 1b4:	2f 67       	addc	@r7,	r15	
 1b6:	63 63       	.word	0x6363;	????	Illegal as 2-op instr
 1b8:	2d 34       	jge	$+92     	;abs 0x214
 1ba:	2e 36       	jge	$-930    	;abs 0xfe18
 1bc:	2e 33       	jn	$-418    	;abs 0x1a
 1be:	2f 67       	addc	@r7,	r15	
 1c0:	63 63       	.word	0x6363;	????	Illegal as 2-op instr
 1c2:	2f 63       	addc	#2,	r15	;r3 As==10
 1c4:	6f 6e       	addc.b	@r14,	r15	
 1c6:	66 69       	addc.b	@r9,	r6	
 1c8:	67 2f       	jc	$-304    	;abs 0x98
 1ca:	6d 73       	subc.b	#2,	r13	;r3 As==10
 1cc:	70 34       	jge	$+226    	;abs 0x2ae
 1ce:	33 30       	jn	$+104    	;abs 0x236
 1d0:	2f 63       	addc	#2,	r15	;r3 As==10
 1d2:	72 74       	subc.b	@r4+,	r2	
 1d4:	30 2e       	jc	$-926    	;abs 0xfe36
 1d6:	53 00       	.word	0x0053;	????	
 1d8:	2f 62       	addc	#4,	r15	;r2 As==10
 1da:	75 69       	addc.b	@r9+,	r5	
 1dc:	6c 64       	addc.b	@r4,	r12	
 1de:	2f 62       	addc	#4,	r15	;r2 As==10
 1e0:	75 69       	addc.b	@r9+,	r5	
 1e2:	6c 64       	addc.b	@r4,	r12	
 1e4:	64 2f       	jc	$-310    	;abs 0xae
 1e6:	67 63       	addc.b	#2,	r7	;r3 As==10
 1e8:	63 2d       	jc	$+712    	;abs 0x4b0
 1ea:	6d 73       	subc.b	#2,	r13	;r3 As==10
 1ec:	70 34       	jge	$+226    	;abs 0x2ce
 1ee:	33 30       	jn	$+104    	;abs 0x256
 1f0:	2d 34       	jge	$+92     	;abs 0x24c
 1f2:	2e 36       	jge	$-930    	;abs 0xfe50
 1f4:	2e 33       	jn	$-418    	;abs 0x52
 1f6:	7e 6d       	addc.b	@r13+,	r14	
 1f8:	73 70       	.word	0x7073;	????	Illegal as 2-op instr
 1fa:	67 63       	addc.b	#2,	r7	;r3 As==10
 1fc:	63 2d       	jc	$+712    	;abs 0x4c4
 1fe:	32 30       	jn	$+102    	;abs 0x264
 200:	31 32       	jn	$-924    	;abs 0xfe64
 202:	30 34       	jge	$+98     	;abs 0x264
 204:	30 36       	jge	$-926    	;abs 0xfe66
 206:	2f 62       	addc	#4,	r15	;r2 As==10
 208:	75 69       	addc.b	@r9+,	r5	
 20a:	6c 64       	addc.b	@r4,	r12	
 20c:	2d 72       	subc	#4,	r13	;r2 As==10
 20e:	65 73       	subc.b	#2,	r5	;r3 As==10
 210:	75 6c       	addc.b	@r12+,	r5	
 212:	74 2f       	jc	$-278    	;abs 0xfc
 214:	67 63       	addc.b	#2,	r7	;r3 As==10
 216:	63 00       	.word	0x0063;	????	
 218:	47 4e       	mov.b	r14,	r7	
 21a:	55 20       	jnz	$+172    	;abs 0x2c6
 21c:	41 53       	add.b	#0,	r1	;r3 As==00
 21e:	20 32       	jn	$-958    	;abs 0xfe60
 220:	2e 32       	jn	$-930    	;abs 0xfe7e
 222:	31 2e       	jc	$-924    	;abs 0xfe86
 224:	31 00       	.word	0x0031;	????	
 226:	01 80       	sub	r0,	r1	
 228:	b4 00       	.word	0x00b4;	????	
 22a:	00 00       	.word	0x0000;	????	
 22c:	02 00       	.word	0x0002;	????	
 22e:	3c 00       	.word	0x003c;	????	
 230:	00 00       	.word	0x0000;	????	
 232:	02 01       	.word	0x0102;	????	
 234:	8e 01       	.word	0x018e;	????	
 236:	00 00       	.word	0x0000;	????	
 238:	28 fc       	and	@r12,	r8	
 23a:	3e fc       	and	@r12+,	r14	
 23c:	2f 62       	addc	#4,	r15	;r2 As==10
 23e:	75 69       	addc.b	@r9+,	r5	
 240:	6c 64       	addc.b	@r4,	r12	
 242:	2f 62       	addc	#4,	r15	;r2 As==10
 244:	75 69       	addc.b	@r9+,	r5	
 246:	6c 64       	addc.b	@r4,	r12	
 248:	64 2f       	jc	$-310    	;abs 0x112
 24a:	67 63       	addc.b	#2,	r7	;r3 As==10
 24c:	63 2d       	jc	$+712    	;abs 0x514
 24e:	6d 73       	subc.b	#2,	r13	;r3 As==10
 250:	70 34       	jge	$+226    	;abs 0x332
 252:	33 30       	jn	$+104    	;abs 0x2ba
 254:	2d 34       	jge	$+92     	;abs 0x2b0
 256:	2e 36       	jge	$-930    	;abs 0xfeb4
 258:	2e 33       	jn	$-418    	;abs 0xb6
 25a:	7e 6d       	addc.b	@r13+,	r14	
 25c:	73 70       	.word	0x7073;	????	Illegal as 2-op instr
 25e:	67 63       	addc.b	#2,	r7	;r3 As==10
 260:	63 2d       	jc	$+712    	;abs 0x528
 262:	32 30       	jn	$+102    	;abs 0x2c8
 264:	31 32       	jn	$-924    	;abs 0xfec8
 266:	30 34       	jge	$+98     	;abs 0x2c8
 268:	30 36       	jge	$-926    	;abs 0xfeca
 26a:	2f 2e       	jc	$-928    	;abs 0xfeca
 26c:	2f 67       	addc	@r7,	r15	
 26e:	63 63       	.word	0x6363;	????	Illegal as 2-op instr
 270:	2d 34       	jge	$+92     	;abs 0x2cc
 272:	2e 36       	jge	$-930    	;abs 0xfed0
 274:	2e 33       	jn	$-418    	;abs 0xd2
 276:	2f 67       	addc	@r7,	r15	
 278:	63 63       	.word	0x6363;	????	Illegal as 2-op instr
 27a:	2f 63       	addc	#2,	r15	;r3 As==10
 27c:	6f 6e       	addc.b	@r14,	r15	
 27e:	66 69       	addc.b	@r9,	r6	
 280:	67 2f       	jc	$-304    	;abs 0x150
 282:	6d 73       	subc.b	#2,	r13	;r3 As==10
 284:	70 34       	jge	$+226    	;abs 0x366
 286:	33 30       	jn	$+104    	;abs 0x2ee
 288:	2f 63       	addc	#2,	r15	;r3 As==10
 28a:	72 74       	subc.b	@r4+,	r2	
 28c:	30 2e       	jc	$-926    	;abs 0xfeee
 28e:	53 00       	.word	0x0053;	????	
 290:	2f 62       	addc	#4,	r15	;r2 As==10
 292:	75 69       	addc.b	@r9+,	r5	
 294:	6c 64       	addc.b	@r4,	r12	
 296:	2f 62       	addc	#4,	r15	;r2 As==10
 298:	75 69       	addc.b	@r9+,	r5	
 29a:	6c 64       	addc.b	@r4,	r12	
 29c:	64 2f       	jc	$-310    	;abs 0x166
 29e:	67 63       	addc.b	#2,	r7	;r3 As==10
 2a0:	63 2d       	jc	$+712    	;abs 0x568
 2a2:	6d 73       	subc.b	#2,	r13	;r3 As==10
 2a4:	70 34       	jge	$+226    	;abs 0x386
 2a6:	33 30       	jn	$+104    	;abs 0x30e
 2a8:	2d 34       	jge	$+92     	;abs 0x304
 2aa:	2e 36       	jge	$-930    	;abs 0xff08
 2ac:	2e 33       	jn	$-418    	;abs 0x10a
 2ae:	7e 6d       	addc.b	@r13+,	r14	
 2b0:	73 70       	.word	0x7073;	????	Illegal as 2-op instr
 2b2:	67 63       	addc.b	#2,	r7	;r3 As==10
 2b4:	63 2d       	jc	$+712    	;abs 0x57c
 2b6:	32 30       	jn	$+102    	;abs 0x31c
 2b8:	31 32       	jn	$-924    	;abs 0xff1c
 2ba:	30 34       	jge	$+98     	;abs 0x31c
 2bc:	30 36       	jge	$-926    	;abs 0xff1e
 2be:	2f 62       	addc	#4,	r15	;r2 As==10
 2c0:	75 69       	addc.b	@r9+,	r5	
 2c2:	6c 64       	addc.b	@r4,	r12	
 2c4:	2d 72       	subc	#4,	r13	;r2 As==10
 2c6:	65 73       	subc.b	#2,	r5	;r3 As==10
 2c8:	75 6c       	addc.b	@r12+,	r5	
 2ca:	74 2f       	jc	$-278    	;abs 0x1b4
 2cc:	67 63       	addc.b	#2,	r7	;r3 As==10
 2ce:	63 00       	.word	0x0063;	????	
 2d0:	47 4e       	mov.b	r14,	r7	
 2d2:	55 20       	jnz	$+172    	;abs 0x37e
 2d4:	41 53       	add.b	#0,	r1	;r3 As==00
 2d6:	20 32       	jn	$-958    	;abs 0xff18
 2d8:	2e 32       	jn	$-930    	;abs 0xff36
 2da:	31 2e       	jc	$-924    	;abs 0xff3e
 2dc:	31 00       	.word	0x0031;	????	
 2de:	01 80       	sub	r0,	r1	
 2e0:	b4 00       	.word	0x00b4;	????	
 2e2:	00 00       	.word	0x0000;	????	
 2e4:	02 00       	.word	0x0002;	????	
 2e6:	50 00       	.word	0x0050;	????	
 2e8:	00 00       	.word	0x0000;	????	
 2ea:	02 01       	.word	0x0102;	????	
 2ec:	16 02       	.word	0x0216;	????	
 2ee:	00 00       	.word	0x0000;	????	
 2f0:	5a fc 60 fc 	and.b	-928(r12),r10	;0xfc60(r12)
 2f4:	2f 62       	addc	#4,	r15	;r2 As==10
 2f6:	75 69       	addc.b	@r9+,	r5	
 2f8:	6c 64       	addc.b	@r4,	r12	
 2fa:	2f 62       	addc	#4,	r15	;r2 As==10
 2fc:	75 69       	addc.b	@r9+,	r5	
 2fe:	6c 64       	addc.b	@r4,	r12	
 300:	64 2f       	jc	$-310    	;abs 0x1ca
 302:	67 63       	addc.b	#2,	r7	;r3 As==10
 304:	63 2d       	jc	$+712    	;abs 0x5cc
 306:	6d 73       	subc.b	#2,	r13	;r3 As==10
 308:	70 34       	jge	$+226    	;abs 0x3ea
 30a:	33 30       	jn	$+104    	;abs 0x372
 30c:	2d 34       	jge	$+92     	;abs 0x368
 30e:	2e 36       	jge	$-930    	;abs 0xff6c
 310:	2e 33       	jn	$-418    	;abs 0x16e
 312:	7e 6d       	addc.b	@r13+,	r14	
 314:	73 70       	.word	0x7073;	????	Illegal as 2-op instr
 316:	67 63       	addc.b	#2,	r7	;r3 As==10
 318:	63 2d       	jc	$+712    	;abs 0x5e0
 31a:	32 30       	jn	$+102    	;abs 0x380
 31c:	31 32       	jn	$-924    	;abs 0xff80
 31e:	30 34       	jge	$+98     	;abs 0x380
 320:	30 36       	jge	$-926    	;abs 0xff82
 322:	2f 2e       	jc	$-928    	;abs 0xff82
 324:	2f 67       	addc	@r7,	r15	
 326:	63 63       	.word	0x6363;	????	Illegal as 2-op instr
 328:	2d 34       	jge	$+92     	;abs 0x384
 32a:	2e 36       	jge	$-930    	;abs 0xff88
 32c:	2e 33       	jn	$-418    	;abs 0x18a
 32e:	2f 67       	addc	@r7,	r15	
 330:	63 63       	.word	0x6363;	????	Illegal as 2-op instr
 332:	2f 63       	addc	#2,	r15	;r3 As==10
 334:	6f 6e       	addc.b	@r14,	r15	
 336:	66 69       	addc.b	@r9,	r6	
 338:	67 2f       	jc	$-304    	;abs 0x208
 33a:	6d 73       	subc.b	#2,	r13	;r3 As==10
 33c:	70 34       	jge	$+226    	;abs 0x41e
 33e:	33 30       	jn	$+104    	;abs 0x3a6
 340:	2f 63       	addc	#2,	r15	;r3 As==10
 342:	72 74       	subc.b	@r4+,	r2	
 344:	30 2e       	jc	$-926    	;abs 0xffa6
 346:	53 00       	.word	0x0053;	????	
 348:	2f 62       	addc	#4,	r15	;r2 As==10
 34a:	75 69       	addc.b	@r9+,	r5	
 34c:	6c 64       	addc.b	@r4,	r12	
 34e:	2f 62       	addc	#4,	r15	;r2 As==10
 350:	75 69       	addc.b	@r9+,	r5	
 352:	6c 64       	addc.b	@r4,	r12	
 354:	64 2f       	jc	$-310    	;abs 0x21e
 356:	67 63       	addc.b	#2,	r7	;r3 As==10
 358:	63 2d       	jc	$+712    	;abs 0x620
 35a:	6d 73       	subc.b	#2,	r13	;r3 As==10
 35c:	70 34       	jge	$+226    	;abs 0x43e
 35e:	33 30       	jn	$+104    	;abs 0x3c6
 360:	2d 34       	jge	$+92     	;abs 0x3bc
 362:	2e 36       	jge	$-930    	;abs 0xffc0
 364:	2e 33       	jn	$-418    	;abs 0x1c2
 366:	7e 6d       	addc.b	@r13+,	r14	
 368:	73 70       	.word	0x7073;	????	Illegal as 2-op instr
 36a:	67 63       	addc.b	#2,	r7	;r3 As==10
 36c:	63 2d       	jc	$+712    	;abs 0x634
 36e:	32 30       	jn	$+102    	;abs 0x3d4
 370:	31 32       	jn	$-924    	;abs 0xffd4
 372:	30 34       	jge	$+98     	;abs 0x3d4
 374:	30 36       	jge	$-926    	;abs 0xffd6
 376:	2f 62       	addc	#4,	r15	;r2 As==10
 378:	75 69       	addc.b	@r9+,	r5	
 37a:	6c 64       	addc.b	@r4,	r12	
 37c:	2d 72       	subc	#4,	r13	;r2 As==10
 37e:	65 73       	subc.b	#2,	r5	;r3 As==10
 380:	75 6c       	addc.b	@r12+,	r5	
 382:	74 2f       	jc	$-278    	;abs 0x26c
 384:	67 63       	addc.b	#2,	r7	;r3 As==10
 386:	63 00       	.word	0x0063;	????	
 388:	47 4e       	mov.b	r14,	r7	
 38a:	55 20       	jnz	$+172    	;abs 0x436
 38c:	41 53       	add.b	#0,	r1	;r3 As==00
 38e:	20 32       	jn	$-958    	;abs 0xffd0
 390:	2e 32       	jn	$-930    	;abs 0xffee
 392:	31 2e       	jc	$-924    	;abs 0xfff6
 394:	31 00       	.word	0x0031;	????	
 396:	01 80       	sub	r0,	r1	
 398:	b4 00       	.word	0x00b4;	????	
 39a:	00 00       	.word	0x0000;	????	
 39c:	02 00       	.word	0x0002;	????	
 39e:	64 00       	.word	0x0064;	????	
 3a0:	00 00       	.word	0x0000;	????	
 3a2:	02 01       	.word	0x0102;	????	
 3a4:	99 02       	.word	0x0299;	????	
 3a6:	00 00       	.word	0x0000;	????	
 3a8:	da fc dc fc 	and.b	-804(r12),25135(r10);0xfcdc(r12), 0x622f(r10)
 3ac:	2f 62 
 3ae:	75 69       	addc.b	@r9+,	r5	
 3b0:	6c 64       	addc.b	@r4,	r12	
 3b2:	2f 62       	addc	#4,	r15	;r2 As==10
 3b4:	75 69       	addc.b	@r9+,	r5	
 3b6:	6c 64       	addc.b	@r4,	r12	
 3b8:	64 2f       	jc	$-310    	;abs 0x282
 3ba:	67 63       	addc.b	#2,	r7	;r3 As==10
 3bc:	63 2d       	jc	$+712    	;abs 0x684
 3be:	6d 73       	subc.b	#2,	r13	;r3 As==10
 3c0:	70 34       	jge	$+226    	;abs 0x4a2
 3c2:	33 30       	jn	$+104    	;abs 0x42a
 3c4:	2d 34       	jge	$+92     	;abs 0x420
 3c6:	2e 36       	jge	$-930    	;abs 0x24
 3c8:	2e 33       	jn	$-418    	;abs 0x226
 3ca:	7e 6d       	addc.b	@r13+,	r14	
 3cc:	73 70       	.word	0x7073;	????	Illegal as 2-op instr
 3ce:	67 63       	addc.b	#2,	r7	;r3 As==10
 3d0:	63 2d       	jc	$+712    	;abs 0x698
 3d2:	32 30       	jn	$+102    	;abs 0x438
 3d4:	31 32       	jn	$-924    	;abs 0x38
 3d6:	30 34       	jge	$+98     	;abs 0x438
 3d8:	30 36       	jge	$-926    	;abs 0x3a
 3da:	2f 2e       	jc	$-928    	;abs 0x3a
 3dc:	2f 67       	addc	@r7,	r15	
 3de:	63 63       	.word	0x6363;	????	Illegal as 2-op instr
 3e0:	2d 34       	jge	$+92     	;abs 0x43c
 3e2:	2e 36       	jge	$-930    	;abs 0x40
 3e4:	2e 33       	jn	$-418    	;abs 0x242
 3e6:	2f 67       	addc	@r7,	r15	
 3e8:	63 63       	.word	0x6363;	????	Illegal as 2-op instr
 3ea:	2f 63       	addc	#2,	r15	;r3 As==10
 3ec:	6f 6e       	addc.b	@r14,	r15	
 3ee:	66 69       	addc.b	@r9,	r6	
 3f0:	67 2f       	jc	$-304    	;abs 0x2c0
 3f2:	6d 73       	subc.b	#2,	r13	;r3 As==10
 3f4:	70 34       	jge	$+226    	;abs 0x4d6
 3f6:	33 30       	jn	$+104    	;abs 0x45e
 3f8:	2f 63       	addc	#2,	r15	;r3 As==10
 3fa:	72 74       	subc.b	@r4+,	r2	
 3fc:	30 2e       	jc	$-926    	;abs 0x5e
 3fe:	53 00       	.word	0x0053;	????	
 400:	2f 62       	addc	#4,	r15	;r2 As==10
 402:	75 69       	addc.b	@r9+,	r5	
 404:	6c 64       	addc.b	@r4,	r12	
 406:	2f 62       	addc	#4,	r15	;r2 As==10
 408:	75 69       	addc.b	@r9+,	r5	
 40a:	6c 64       	addc.b	@r4,	r12	
 40c:	64 2f       	jc	$-310    	;abs 0x2d6
 40e:	67 63       	addc.b	#2,	r7	;r3 As==10
 410:	63 2d       	jc	$+712    	;abs 0x6d8
 412:	6d 73       	subc.b	#2,	r13	;r3 As==10
 414:	70 34       	jge	$+226    	;abs 0x4f6
 416:	33 30       	jn	$+104    	;abs 0x47e
 418:	2d 34       	jge	$+92     	;abs 0x474
 41a:	2e 36       	jge	$-930    	;abs 0x78
 41c:	2e 33       	jn	$-418    	;abs 0x27a
 41e:	7e 6d       	addc.b	@r13+,	r14	
 420:	73 70       	.word	0x7073;	????	Illegal as 2-op instr
 422:	67 63       	addc.b	#2,	r7	;r3 As==10
 424:	63 2d       	jc	$+712    	;abs 0x6ec
 426:	32 30       	jn	$+102    	;abs 0x48c
 428:	31 32       	jn	$-924    	;abs 0x8c
 42a:	30 34       	jge	$+98     	;abs 0x48c
 42c:	30 36       	jge	$-926    	;abs 0x8e
 42e:	2f 62       	addc	#4,	r15	;r2 As==10
 430:	75 69       	addc.b	@r9+,	r5	
 432:	6c 64       	addc.b	@r4,	r12	
 434:	2d 72       	subc	#4,	r13	;r2 As==10
 436:	65 73       	subc.b	#2,	r5	;r3 As==10
 438:	75 6c       	addc.b	@r12+,	r5	
 43a:	74 2f       	jc	$-278    	;abs 0x324
 43c:	67 63       	addc.b	#2,	r7	;r3 As==10
 43e:	63 00       	.word	0x0063;	????	
 440:	47 4e       	mov.b	r14,	r7	
 442:	55 20       	jnz	$+172    	;abs 0x4ee
 444:	41 53       	add.b	#0,	r1	;r3 As==00
 446:	20 32       	jn	$-958    	;abs 0x88
 448:	2e 32       	jn	$-930    	;abs 0xa6
 44a:	31 2e       	jc	$-924    	;abs 0xae
 44c:	31 00       	.word	0x0031;	????	
 44e:	01 80       	sub	r0,	r1	

Disassembly of section .debug_abbrev:

00000000 <.debug_abbrev>:
   0:	01 11       	rra	r1		
   2:	00 10       	rrc	r0		
   4:	06 11       	rra	r6		
   6:	01 12       	push	r1		
   8:	01 03       	.word	0x0301;	????	
   a:	08 1b       	.word	0x1b08;	????	
   c:	08 25       	jz	$+530    	;abs 0x21e
   e:	08 13       	reti			;return from interupt
  10:	05 00       	.word	0x0005;	????	
  12:	00 00       	.word	0x0000;	????	
  14:	01 11       	rra	r1		
  16:	00 10       	rrc	r0		
  18:	06 11       	rra	r6		
  1a:	01 12       	push	r1		
  1c:	01 03       	.word	0x0301;	????	
  1e:	08 1b       	.word	0x1b08;	????	
  20:	08 25       	jz	$+530    	;abs 0x232
  22:	08 13       	reti			;return from interupt
  24:	05 00       	.word	0x0005;	????	
  26:	00 00       	.word	0x0000;	????	
  28:	01 11       	rra	r1		
  2a:	00 10       	rrc	r0		
  2c:	06 11       	rra	r6		
  2e:	01 12       	push	r1		
  30:	01 03       	.word	0x0301;	????	
  32:	08 1b       	.word	0x1b08;	????	
  34:	08 25       	jz	$+530    	;abs 0x246
  36:	08 13       	reti			;return from interupt
  38:	05 00       	.word	0x0005;	????	
  3a:	00 00       	.word	0x0000;	????	
  3c:	01 11       	rra	r1		
  3e:	00 10       	rrc	r0		
  40:	06 11       	rra	r6		
  42:	01 12       	push	r1		
  44:	01 03       	.word	0x0301;	????	
  46:	08 1b       	.word	0x1b08;	????	
  48:	08 25       	jz	$+530    	;abs 0x25a
  4a:	08 13       	reti			;return from interupt
  4c:	05 00       	.word	0x0005;	????	
  4e:	00 00       	.word	0x0000;	????	
  50:	01 11       	rra	r1		
  52:	00 10       	rrc	r0		
  54:	06 11       	rra	r6		
  56:	01 12       	push	r1		
  58:	01 03       	.word	0x0301;	????	
  5a:	08 1b       	.word	0x1b08;	????	
  5c:	08 25       	jz	$+530    	;abs 0x26e
  5e:	08 13       	reti			;return from interupt
  60:	05 00       	.word	0x0005;	????	
  62:	00 00       	.word	0x0000;	????	
  64:	01 11       	rra	r1		
  66:	00 10       	rrc	r0		
  68:	06 11       	rra	r6		
  6a:	01 12       	push	r1		
  6c:	01 03       	.word	0x0301;	????	
  6e:	08 1b       	.word	0x1b08;	????	
  70:	08 25       	jz	$+530    	;abs 0x282
  72:	08 13       	reti			;return from interupt
  74:	05 00       	.word	0x0005;	????	
	...

Disassembly of section .debug_line:

00000000 <.debug_line>:
   0:	80 00       	.word	0x0080;	????	
   2:	00 00       	.word	0x0000;	????	
   4:	02 00       	.word	0x0002;	????	
   6:	6a 00       	.word	0x006a;	????	
   8:	00 00       	.word	0x0000;	????	
   a:	01 01       	.word	0x0101;	????	
   c:	fb 0e       	.word	0x0efb;	????	
   e:	0d 00       	.word	0x000d;	????	
  10:	01 01       	.word	0x0101;	????	
  12:	01 01       	.word	0x0101;	????	
  14:	00 00       	.word	0x0000;	????	
  16:	00 01       	.word	0x0100;	????	
  18:	00 00       	.word	0x0000;	????	
  1a:	01 2f       	jc	$-508    	;abs 0xfe1e
  1c:	62 75       	subc.b	@r5,	r2	
  1e:	69 6c       	addc.b	@r12,	r9	
  20:	64 2f       	jc	$-310    	;abs 0xfeea
  22:	62 75       	subc.b	@r5,	r2	
  24:	69 6c       	addc.b	@r12,	r9	
  26:	64 64       	addc.b	@r4,	r4	
  28:	2f 67       	addc	@r7,	r15	
  2a:	63 63       	.word	0x6363;	????	Illegal as 2-op instr
  2c:	2d 6d       	addc	@r13,	r13	
  2e:	73 70       	.word	0x7073;	????	Illegal as 2-op instr
  30:	34 33       	jn	$-406    	;abs 0xfe9a
  32:	30 2d       	jc	$+610    	;abs 0x294
  34:	34 2e       	jc	$-918    	;abs 0xfc9e
  36:	36 2e       	jc	$-914    	;abs 0xfca4
  38:	33 7e       	.word	0x7e33;	????	Illegal as 2-op instr
  3a:	6d 73       	subc.b	#2,	r13	;r3 As==10
  3c:	70 67       	addc.b	@r7+,	r0	
  3e:	63 63       	.word	0x6363;	????	Illegal as 2-op instr
  40:	2d 32       	jn	$-932    	;abs 0xfc9c
  42:	30 31       	jn	$+610    	;abs 0x2a4
  44:	32 30       	jn	$+102    	;abs 0xaa
  46:	34 30       	jn	$+106    	;abs 0xb0
  48:	36 2f       	jc	$-402    	;abs 0xfeb6
  4a:	2e 2f       	jc	$-418    	;abs 0xfea8
  4c:	67 63       	addc.b	#2,	r7	;r3 As==10
  4e:	63 2d       	jc	$+712    	;abs 0x316
  50:	34 2e       	jc	$-918    	;abs 0xfcba
  52:	36 2e       	jc	$-914    	;abs 0xfcc0
  54:	33 2f       	jc	$-408    	;abs 0xfebc
  56:	67 63       	addc.b	#2,	r7	;r3 As==10
  58:	63 2f       	jc	$-312    	;abs 0xff20
  5a:	63 6f       	.word	0x6f63;	????	Illegal as 2-op instr
  5c:	6e 66       	addc.b	@r6,	r14	
  5e:	69 67       	addc.b	@r7,	r9	
  60:	2f 6d       	addc	@r13,	r15	
  62:	73 70       	.word	0x7073;	????	Illegal as 2-op instr
  64:	34 33       	jn	$-406    	;abs 0xfece
  66:	30 00       	.word	0x0030;	????	
  68:	00 63       	adc	r0		
  6a:	72 74       	subc.b	@r4+,	r2	
  6c:	30 2e       	jc	$-926    	;abs 0xfcce
  6e:	53 00       	.word	0x0053;	????	
  70:	01 00       	.word	0x0001;	????	
  72:	00 00       	.word	0x0000;	????	
  74:	00 03       	.word	0x0300;	????	
  76:	02 00       	.word	0x0002;	????	
  78:	fc 03       	.word	0x03fc;	????	
  7a:	e8 00       	.word	0x00e8;	????	
  7c:	01 4b       	mov	r11,	r1	
  7e:	4b 02       	.word	0x024b;	????	
  80:	04 00       	.word	0x0004;	????	
  82:	01 01       	.word	0x0101;	????	
  84:	7e 00       	.word	0x007e;	????	
  86:	00 00       	.word	0x0000;	????	
  88:	02 00       	.word	0x0002;	????	
  8a:	6a 00       	.word	0x006a;	????	
  8c:	00 00       	.word	0x0000;	????	
  8e:	01 01       	.word	0x0101;	????	
  90:	fb 0e       	.word	0x0efb;	????	
  92:	0d 00       	.word	0x000d;	????	
  94:	01 01       	.word	0x0101;	????	
  96:	01 01       	.word	0x0101;	????	
  98:	00 00       	.word	0x0000;	????	
  9a:	00 01       	.word	0x0100;	????	
  9c:	00 00       	.word	0x0000;	????	
  9e:	01 2f       	jc	$-508    	;abs 0xfea2
  a0:	62 75       	subc.b	@r5,	r2	
  a2:	69 6c       	addc.b	@r12,	r9	
  a4:	64 2f       	jc	$-310    	;abs 0xff6e
  a6:	62 75       	subc.b	@r5,	r2	
  a8:	69 6c       	addc.b	@r12,	r9	
  aa:	64 64       	addc.b	@r4,	r4	
  ac:	2f 67       	addc	@r7,	r15	
  ae:	63 63       	.word	0x6363;	????	Illegal as 2-op instr
  b0:	2d 6d       	addc	@r13,	r13	
  b2:	73 70       	.word	0x7073;	????	Illegal as 2-op instr
  b4:	34 33       	jn	$-406    	;abs 0xff1e
  b6:	30 2d       	jc	$+610    	;abs 0x318
  b8:	34 2e       	jc	$-918    	;abs 0xfd22
  ba:	36 2e       	jc	$-914    	;abs 0xfd28
  bc:	33 7e       	.word	0x7e33;	????	Illegal as 2-op instr
  be:	6d 73       	subc.b	#2,	r13	;r3 As==10
  c0:	70 67       	addc.b	@r7+,	r0	
  c2:	63 63       	.word	0x6363;	????	Illegal as 2-op instr
  c4:	2d 32       	jn	$-932    	;abs 0xfd20
  c6:	30 31       	jn	$+610    	;abs 0x328
  c8:	32 30       	jn	$+102    	;abs 0x12e
  ca:	34 30       	jn	$+106    	;abs 0x134
  cc:	36 2f       	jc	$-402    	;abs 0xff3a
  ce:	2e 2f       	jc	$-418    	;abs 0xff2c
  d0:	67 63       	addc.b	#2,	r7	;r3 As==10
  d2:	63 2d       	jc	$+712    	;abs 0x39a
  d4:	34 2e       	jc	$-918    	;abs 0xfd3e
  d6:	36 2e       	jc	$-914    	;abs 0xfd44
  d8:	33 2f       	jc	$-408    	;abs 0xff40
  da:	67 63       	addc.b	#2,	r7	;r3 As==10
  dc:	63 2f       	jc	$-312    	;abs 0xffa4
  de:	63 6f       	.word	0x6f63;	????	Illegal as 2-op instr
  e0:	6e 66       	addc.b	@r6,	r14	
  e2:	69 67       	addc.b	@r7,	r9	
  e4:	2f 6d       	addc	@r13,	r15	
  e6:	73 70       	.word	0x7073;	????	Illegal as 2-op instr
  e8:	34 33       	jn	$-406    	;abs 0xff52
  ea:	30 00       	.word	0x0030;	????	
  ec:	00 63       	adc	r0		
  ee:	72 74       	subc.b	@r4+,	r2	
  f0:	30 2e       	jc	$-926    	;abs 0xfd52
  f2:	53 00       	.word	0x0053;	????	
  f4:	01 00       	.word	0x0001;	????	
  f6:	00 00       	.word	0x0000;	????	
  f8:	00 03       	.word	0x0300;	????	
  fa:	02 0c       	.word	0x0c02;	????	
  fc:	fc 03       	.word	0x03fc;	????	
  fe:	99 01       	.word	0x0199;	????	
 100:	01 02       	.word	0x0201;	????	
 102:	04 00       	.word	0x0004;	????	
 104:	01 01       	.word	0x0101;	????	
 106:	84 00       	.word	0x0084;	????	
 108:	00 00       	.word	0x0000;	????	
 10a:	02 00       	.word	0x0002;	????	
 10c:	6a 00       	.word	0x006a;	????	
 10e:	00 00       	.word	0x0000;	????	
 110:	01 01       	.word	0x0101;	????	
 112:	fb 0e       	.word	0x0efb;	????	
 114:	0d 00       	.word	0x000d;	????	
 116:	01 01       	.word	0x0101;	????	
 118:	01 01       	.word	0x0101;	????	
 11a:	00 00       	.word	0x0000;	????	
 11c:	00 01       	.word	0x0100;	????	
 11e:	00 00       	.word	0x0000;	????	
 120:	01 2f       	jc	$-508    	;abs 0xff24
 122:	62 75       	subc.b	@r5,	r2	
 124:	69 6c       	addc.b	@r12,	r9	
 126:	64 2f       	jc	$-310    	;abs 0xfff0
 128:	62 75       	subc.b	@r5,	r2	
 12a:	69 6c       	addc.b	@r12,	r9	
 12c:	64 64       	addc.b	@r4,	r4	
 12e:	2f 67       	addc	@r7,	r15	
 130:	63 63       	.word	0x6363;	????	Illegal as 2-op instr
 132:	2d 6d       	addc	@r13,	r13	
 134:	73 70       	.word	0x7073;	????	Illegal as 2-op instr
 136:	34 33       	jn	$-406    	;abs 0xffa0
 138:	30 2d       	jc	$+610    	;abs 0x39a
 13a:	34 2e       	jc	$-918    	;abs 0xfda4
 13c:	36 2e       	jc	$-914    	;abs 0xfdaa
 13e:	33 7e       	.word	0x7e33;	????	Illegal as 2-op instr
 140:	6d 73       	subc.b	#2,	r13	;r3 As==10
 142:	70 67       	addc.b	@r7+,	r0	
 144:	63 63       	.word	0x6363;	????	Illegal as 2-op instr
 146:	2d 32       	jn	$-932    	;abs 0xfda2
 148:	30 31       	jn	$+610    	;abs 0x3aa
 14a:	32 30       	jn	$+102    	;abs 0x1b0
 14c:	34 30       	jn	$+106    	;abs 0x1b6
 14e:	36 2f       	jc	$-402    	;abs 0xffbc
 150:	2e 2f       	jc	$-418    	;abs 0xffae
 152:	67 63       	addc.b	#2,	r7	;r3 As==10
 154:	63 2d       	jc	$+712    	;abs 0x41c
 156:	34 2e       	jc	$-918    	;abs 0xfdc0
 158:	36 2e       	jc	$-914    	;abs 0xfdc6
 15a:	33 2f       	jc	$-408    	;abs 0xffc2
 15c:	67 63       	addc.b	#2,	r7	;r3 As==10
 15e:	63 2f       	jc	$-312    	;abs 0x26
 160:	63 6f       	.word	0x6f63;	????	Illegal as 2-op instr
 162:	6e 66       	addc.b	@r6,	r14	
 164:	69 67       	addc.b	@r7,	r9	
 166:	2f 6d       	addc	@r13,	r15	
 168:	73 70       	.word	0x7073;	????	Illegal as 2-op instr
 16a:	34 33       	jn	$-406    	;abs 0xffd4
 16c:	30 00       	.word	0x0030;	????	
 16e:	00 63       	adc	r0		
 170:	72 74       	subc.b	@r4+,	r2	
 172:	30 2e       	jc	$-926    	;abs 0xfdd4
 174:	53 00       	.word	0x0053;	????	
 176:	01 00       	.word	0x0001;	????	
 178:	00 00       	.word	0x0000;	????	
 17a:	00 03       	.word	0x0300;	????	
 17c:	02 10       	rrc	r2		
 17e:	fc 03       	.word	0x03fc;	????	
 180:	bc 01       	.word	0x01bc;	????	
 182:	01 4b       	mov	r11,	r1	
 184:	2f 30       	jn	$+96     	;abs 0x1e4
 186:	67 2f       	jc	$-304    	;abs 0x56
 188:	67 02       	.word	0x0267;	????	
 18a:	02 00       	.word	0x0002;	????	
 18c:	01 01       	.word	0x0101;	????	
 18e:	84 00       	.word	0x0084;	????	
 190:	00 00       	.word	0x0000;	????	
 192:	02 00       	.word	0x0002;	????	
 194:	6a 00       	.word	0x006a;	????	
 196:	00 00       	.word	0x0000;	????	
 198:	01 01       	.word	0x0101;	????	
 19a:	fb 0e       	.word	0x0efb;	????	
 19c:	0d 00       	.word	0x000d;	????	
 19e:	01 01       	.word	0x0101;	????	
 1a0:	01 01       	.word	0x0101;	????	
 1a2:	00 00       	.word	0x0000;	????	
 1a4:	00 01       	.word	0x0100;	????	
 1a6:	00 00       	.word	0x0000;	????	
 1a8:	01 2f       	jc	$-508    	;abs 0xffac
 1aa:	62 75       	subc.b	@r5,	r2	
 1ac:	69 6c       	addc.b	@r12,	r9	
 1ae:	64 2f       	jc	$-310    	;abs 0x78
 1b0:	62 75       	subc.b	@r5,	r2	
 1b2:	69 6c       	addc.b	@r12,	r9	
 1b4:	64 64       	addc.b	@r4,	r4	
 1b6:	2f 67       	addc	@r7,	r15	
 1b8:	63 63       	.word	0x6363;	????	Illegal as 2-op instr
 1ba:	2d 6d       	addc	@r13,	r13	
 1bc:	73 70       	.word	0x7073;	????	Illegal as 2-op instr
 1be:	34 33       	jn	$-406    	;abs 0x28
 1c0:	30 2d       	jc	$+610    	;abs 0x422
 1c2:	34 2e       	jc	$-918    	;abs 0xfe2c
 1c4:	36 2e       	jc	$-914    	;abs 0xfe32
 1c6:	33 7e       	.word	0x7e33;	????	Illegal as 2-op instr
 1c8:	6d 73       	subc.b	#2,	r13	;r3 As==10
 1ca:	70 67       	addc.b	@r7+,	r0	
 1cc:	63 63       	.word	0x6363;	????	Illegal as 2-op instr
 1ce:	2d 32       	jn	$-932    	;abs 0xfe2a
 1d0:	30 31       	jn	$+610    	;abs 0x432
 1d2:	32 30       	jn	$+102    	;abs 0x238
 1d4:	34 30       	jn	$+106    	;abs 0x23e
 1d6:	36 2f       	jc	$-402    	;abs 0x44
 1d8:	2e 2f       	jc	$-418    	;abs 0x36
 1da:	67 63       	addc.b	#2,	r7	;r3 As==10
 1dc:	63 2d       	jc	$+712    	;abs 0x4a4
 1de:	34 2e       	jc	$-918    	;abs 0xfe48
 1e0:	36 2e       	jc	$-914    	;abs 0xfe4e
 1e2:	33 2f       	jc	$-408    	;abs 0x4a
 1e4:	67 63       	addc.b	#2,	r7	;r3 As==10
 1e6:	63 2f       	jc	$-312    	;abs 0xae
 1e8:	63 6f       	.word	0x6f63;	????	Illegal as 2-op instr
 1ea:	6e 66       	addc.b	@r6,	r14	
 1ec:	69 67       	addc.b	@r7,	r9	
 1ee:	2f 6d       	addc	@r13,	r15	
 1f0:	73 70       	.word	0x7073;	????	Illegal as 2-op instr
 1f2:	34 33       	jn	$-406    	;abs 0x5c
 1f4:	30 00       	.word	0x0030;	????	
 1f6:	00 63       	adc	r0		
 1f8:	72 74       	subc.b	@r4+,	r2	
 1fa:	30 2e       	jc	$-926    	;abs 0xfe5c
 1fc:	53 00       	.word	0x0053;	????	
 1fe:	01 00       	.word	0x0001;	????	
 200:	00 00       	.word	0x0000;	????	
 202:	00 03       	.word	0x0300;	????	
 204:	02 28       	jnc	$+6      	;abs 0x20a
 206:	fc 03       	.word	0x03fc;	????	
 208:	d6 01       	.word	0x01d6;	????	
 20a:	01 4b       	mov	r11,	r1	
 20c:	2f 30       	jn	$+96     	;abs 0x26c
 20e:	67 2f       	jc	$-304    	;abs 0xde
 210:	4b 02       	.word	0x024b;	????	
 212:	02 00       	.word	0x0002;	????	
 214:	01 01       	.word	0x0101;	????	
 216:	7f 00       	.word	0x007f;	????	
 218:	00 00       	.word	0x0000;	????	
 21a:	02 00       	.word	0x0002;	????	
 21c:	6a 00       	.word	0x006a;	????	
 21e:	00 00       	.word	0x0000;	????	
 220:	01 01       	.word	0x0101;	????	
 222:	fb 0e       	.word	0x0efb;	????	
 224:	0d 00       	.word	0x000d;	????	
 226:	01 01       	.word	0x0101;	????	
 228:	01 01       	.word	0x0101;	????	
 22a:	00 00       	.word	0x0000;	????	
 22c:	00 01       	.word	0x0100;	????	
 22e:	00 00       	.word	0x0000;	????	
 230:	01 2f       	jc	$-508    	;abs 0x34
 232:	62 75       	subc.b	@r5,	r2	
 234:	69 6c       	addc.b	@r12,	r9	
 236:	64 2f       	jc	$-310    	;abs 0x100
 238:	62 75       	subc.b	@r5,	r2	
 23a:	69 6c       	addc.b	@r12,	r9	
 23c:	64 64       	addc.b	@r4,	r4	
 23e:	2f 67       	addc	@r7,	r15	
 240:	63 63       	.word	0x6363;	????	Illegal as 2-op instr
 242:	2d 6d       	addc	@r13,	r13	
 244:	73 70       	.word	0x7073;	????	Illegal as 2-op instr
 246:	34 33       	jn	$-406    	;abs 0xb0
 248:	30 2d       	jc	$+610    	;abs 0x4aa
 24a:	34 2e       	jc	$-918    	;abs 0xfeb4
 24c:	36 2e       	jc	$-914    	;abs 0xfeba
 24e:	33 7e       	.word	0x7e33;	????	Illegal as 2-op instr
 250:	6d 73       	subc.b	#2,	r13	;r3 As==10
 252:	70 67       	addc.b	@r7+,	r0	
 254:	63 63       	.word	0x6363;	????	Illegal as 2-op instr
 256:	2d 32       	jn	$-932    	;abs 0xfeb2
 258:	30 31       	jn	$+610    	;abs 0x4ba
 25a:	32 30       	jn	$+102    	;abs 0x2c0
 25c:	34 30       	jn	$+106    	;abs 0x2c6
 25e:	36 2f       	jc	$-402    	;abs 0xcc
 260:	2e 2f       	jc	$-418    	;abs 0xbe
 262:	67 63       	addc.b	#2,	r7	;r3 As==10
 264:	63 2d       	jc	$+712    	;abs 0x52c
 266:	34 2e       	jc	$-918    	;abs 0xfed0
 268:	36 2e       	jc	$-914    	;abs 0xfed6
 26a:	33 2f       	jc	$-408    	;abs 0xd2
 26c:	67 63       	addc.b	#2,	r7	;r3 As==10
 26e:	63 2f       	jc	$-312    	;abs 0x136
 270:	63 6f       	.word	0x6f63;	????	Illegal as 2-op instr
 272:	6e 66       	addc.b	@r6,	r14	
 274:	69 67       	addc.b	@r7,	r9	
 276:	2f 6d       	addc	@r13,	r15	
 278:	73 70       	.word	0x7073;	????	Illegal as 2-op instr
 27a:	34 33       	jn	$-406    	;abs 0xe4
 27c:	30 00       	.word	0x0030;	????	
 27e:	00 63       	adc	r0		
 280:	72 74       	subc.b	@r4+,	r2	
 282:	30 2e       	jc	$-926    	;abs 0xfee4
 284:	53 00       	.word	0x0053;	????	
 286:	01 00       	.word	0x0001;	????	
 288:	00 00       	.word	0x0000;	????	
 28a:	00 03       	.word	0x0300;	????	
 28c:	02 5a       	add	r10,	r2	
 28e:	fc 03       	.word	0x03fc;	????	
 290:	fe 01       	.word	0x01fe;	????	
 292:	01 4b       	mov	r11,	r1	
 294:	02 02       	.word	0x0202;	????	
 296:	00 01       	.word	0x0100;	????	
 298:	01 7e       	subc	r14,	r1	
 29a:	00 00       	.word	0x0000;	????	
 29c:	00 02       	.word	0x0200;	????	
 29e:	00 6a       	addc	r10,	r0	
 2a0:	00 00       	.word	0x0000;	????	
 2a2:	00 01       	.word	0x0100;	????	
 2a4:	01 fb       	and	r11,	r1	
 2a6:	0e 0d       	.word	0x0d0e;	????	
 2a8:	00 01       	.word	0x0100;	????	
 2aa:	01 01       	.word	0x0101;	????	
 2ac:	01 00       	.word	0x0001;	????	
 2ae:	00 00       	.word	0x0000;	????	
 2b0:	01 00       	.word	0x0001;	????	
 2b2:	00 01       	.word	0x0100;	????	
 2b4:	2f 62       	addc	#4,	r15	;r2 As==10
 2b6:	75 69       	addc.b	@r9+,	r5	
 2b8:	6c 64       	addc.b	@r4,	r12	
 2ba:	2f 62       	addc	#4,	r15	;r2 As==10
 2bc:	75 69       	addc.b	@r9+,	r5	
 2be:	6c 64       	addc.b	@r4,	r12	
 2c0:	64 2f       	jc	$-310    	;abs 0x18a
 2c2:	67 63       	addc.b	#2,	r7	;r3 As==10
 2c4:	63 2d       	jc	$+712    	;abs 0x58c
 2c6:	6d 73       	subc.b	#2,	r13	;r3 As==10
 2c8:	70 34       	jge	$+226    	;abs 0x3aa
 2ca:	33 30       	jn	$+104    	;abs 0x332
 2cc:	2d 34       	jge	$+92     	;abs 0x328
 2ce:	2e 36       	jge	$-930    	;abs 0xff2c
 2d0:	2e 33       	jn	$-418    	;abs 0x12e
 2d2:	7e 6d       	addc.b	@r13+,	r14	
 2d4:	73 70       	.word	0x7073;	????	Illegal as 2-op instr
 2d6:	67 63       	addc.b	#2,	r7	;r3 As==10
 2d8:	63 2d       	jc	$+712    	;abs 0x5a0
 2da:	32 30       	jn	$+102    	;abs 0x340
 2dc:	31 32       	jn	$-924    	;abs 0xff40
 2de:	30 34       	jge	$+98     	;abs 0x340
 2e0:	30 36       	jge	$-926    	;abs 0xff42
 2e2:	2f 2e       	jc	$-928    	;abs 0xff42
 2e4:	2f 67       	addc	@r7,	r15	
 2e6:	63 63       	.word	0x6363;	????	Illegal as 2-op instr
 2e8:	2d 34       	jge	$+92     	;abs 0x344
 2ea:	2e 36       	jge	$-930    	;abs 0xff48
 2ec:	2e 33       	jn	$-418    	;abs 0x14a
 2ee:	2f 67       	addc	@r7,	r15	
 2f0:	63 63       	.word	0x6363;	????	Illegal as 2-op instr
 2f2:	2f 63       	addc	#2,	r15	;r3 As==10
 2f4:	6f 6e       	addc.b	@r14,	r15	
 2f6:	66 69       	addc.b	@r9,	r6	
 2f8:	67 2f       	jc	$-304    	;abs 0x1c8
 2fa:	6d 73       	subc.b	#2,	r13	;r3 As==10
 2fc:	70 34       	jge	$+226    	;abs 0x3de
 2fe:	33 30       	jn	$+104    	;abs 0x366
 300:	00 00       	.word	0x0000;	????	
 302:	63 72       	.word	0x7263;	????	Illegal as 2-op instr
 304:	74 30       	jn	$+234    	;abs 0x3ee
 306:	2e 53       	incd	r14		
 308:	00 01       	.word	0x0100;	????	
 30a:	00 00       	.word	0x0000;	????	
 30c:	00 00       	.word	0x0000;	????	
 30e:	03 02       	.word	0x0203;	????	
 310:	da fc 03 cd 	and.b	-13053(r12),258(r10);0xcd03(r12), 0x0102(r10)
 314:	02 01 
 316:	02 02       	.word	0x0202;	????	
 318:	00 01       	.word	0x0100;	????	
 31a:	01 30       	Address 0x000000000000031a is out of bounds.
Address 0x000000000000031c is out of bounds.
and.b	@r15+,	-1(r15)	;0xffff(r15)
