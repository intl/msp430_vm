#include <stdint.h>
#include <intrinsics.h>
#include <msp430f1121.h>

#pragma vector=0
__interrupt void test_irq_0(void)
{
    P1OUT ^= BIT6;
    P1IFG &=~BIT3;
}

void p1_init(void)
{
P1OUT &= 0x00;               // Shut down everything
P1DIR &= 0x00;               
P1DIR |= BIT0 + BIT6;       // P1.0 and P1.6 pins output the rest are input 
P1OUT |= BIT3;                 //Select pull-up mode for P1.3

P1IE |= BIT3;                    // P1.3 interrupt enabled
P1IES |= BIT3;                  // P1.3 Hi/lo edge
P1IFG &= ~BIT3;
}


int main()
{
p1_init();
uint8_t j = 0;
__eint();
while(1)                      
{
    P1OUT = ++j;
}

return 0;
}

